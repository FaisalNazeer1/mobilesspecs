<?php ob_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<title>Mobile Planet</title>

	<!-- Meta -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	<!-- Favicon -->
	<link rel="shortcut icon" href="favicon.ico">

	<!-- Web Fonts -->
	<link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,800&amp;subset=cyrillic,latin'>

	<!-- CSS Global Compulsory -->
	<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/shop.style.css">
	<link rel="stylesheet" href="assets/css/style.css">

	<!-- CSS Header and Footer -->
	<link rel="stylesheet" href="assets/css/headers/header-v5.css">
	<link rel="stylesheet" href="assets/css/footers/footer-v4.css">

	<!-- CSS Implementing Plugins -->
	<link rel="stylesheet" href="assets/plugins/animate.css">
	<link rel="stylesheet" href="assets/plugins/line-icons/line-icons.css">
	<link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/plugins/scrollbar/css/jquery.mCustomScrollbar.css">
	<link rel="stylesheet" href="assets/plugins/owl-carousel/owl-carousel/owl.carousel.css">
	<link rel="stylesheet" href="assets/plugins/revolution-slider/rs-plugin/css/settings.css">
	<link rel="stylesheet" href="assets/plugins/sky-forms-pro/skyforms/css/sky-forms.css">
	<link rel="stylesheet" href="assets/plugins/sky-forms-pro/skyforms/custom/custom-sky-forms.css">
	<!-- CSS Theme -->
	<link rel="stylesheet" href="assets/css/theme-colors/default.css" id="style_color">
	<link rel="stylesheet" href="assets/plugins/ladda-buttons/css/custom-lada-btn.css">
	<link rel="stylesheet" href="assets/plugins/hover-effects/css/custom-hover-effects.css">

	<!-- CSS Customization -->
	<link rel="stylesheet" href="assets/css/custom.css">
<style type="text/css">
.price {
	font-weight: 700 !important;
	color: #999;
	font-size: 16px;
	width: 220px !important;
	border: 2px solid #999;
	border-radius: 4px;
}
.price:hover {
	color: darkred;
	border: 2px solid green;
	border-radius: 8px;
}
.input {
	margin-left: 40px !important;
}
.label {
	margin-left: 40px !important;
	font-size: 18px !important;
	font-weight: 600 !important;
}
.round-corners2 {
	border-radius: 8px !important;
	background-color: darkgreen !important;
	color: white !important;
	font-size: 28px !important;
	height: 60px !important;
	margin-left: 170px;
	margin-top: 40px;
	width: 200px;
	padding-top: -20px !important;
	padding-bottom: 20px !important;
}
.round-corners2:hover {
	border-radius: 16px !important;
	background-color: darkred !important;
}
</style>
</head>

<body class="header-fixed">

	<div class="wrapper">
						
	<?php include("header.php"); ?>
		<div class="content-md">
 		<?php include("sidebar.php"); ?> 
 			<div class="col-md-9">
 				<div class="filter-results">
 					<div class="row illustration-v2 margin-bottom-30">
 						<form class="sky-form" action="delete_mobile.php" method="POST" enctype="multipart/form-data">
 						<header>ENTER MOBILE DETAILS BELOW (<span style="color: red; font-weight: 600;">TO DELETE</span>)</header>
 							<div class="col-md-6">
 							
								<label class="label">Mobile Name</label>
								<label class="input">
									<input type="text" name="dev_name" id="mobile_name">
								</label>

								<input type="submit" name="submit" value="Submit" class="btn-u btn-u-red round-corners2 btn-u-lg">
							</div>
 						</form>	
 					</div>
 				</div><!--/end filter resilts-->
 			</div>
 		</div><!--/end row-->

	<?php include("footer.php"); ?>
</div><!--/wrapper-->

<!-- JS Global Compulsory -->
<script src="assets/plugins/jquery/jquery.min.js"></script>
<script src="assets/plugins/jquery/jquery-migrate.min.js"></script>
<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/plugins/noUiSlider/jquery.nouislider.all.min.js"></script>
<!-- JS Implementing Plugins -->
<script src="assets/plugins/back-to-top.js"></script>
<script src="assets/plugins/smoothScroll.js"></script>
<script src="assets/plugins/jquery.parallax.js"></script>
<script src="assets/plugins/owl-carousel/owl-carousel/owl.carousel.js"></script>
<script src="assets/plugins/scrollbar/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="assets/plugins/revolution-slider/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script src="assets/plugins/revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script src="assets/plugins/sky-forms-pro/skyforms/js/jquery-ui.min.js"></script>
<!-- JS Customization -->
<script src="assets/js/custom.js"></script>
<!-- JS Page Level -->
<script src="assets/js/shop.app.js"></script>
<script src="assets/js/plugins/owl-carousel.js"></script>
<script src="assets/js/plugins/revolution-slider.js"></script>
<script src="assets/js/plugins/style-switcher.js"></script>
<script type="text/javascript" src="assets/js/plugins/ladda-buttons.js"></script>
<script>
	jQuery(document).ready(function() {
		App.init();
		App.initScrollBar();
		App.initParallaxBg();
		OwlCarousel.initOwlCarousel();
		RevolutionSlider.initRSfullWidth();
		//StyleSwitcher.initStyleSwitcher();
	});
</script>

<?php
if (isset($_POST['submit'])) {

include("connect.php");

$dev_name = $_POST['dev_name'];

$result = mysqli_query($con, "DELETE FROM mobiles WHERE dev_name = '$dev_name' LIMIT 1");

$result = mysqli_query($con, $sql);

if ($result) {
	header("delete_mobile.php?msg=success");

}else{
	echo "ERROR: "; echo mysqli_error($con);
}

}
?>
</body>

</html>
