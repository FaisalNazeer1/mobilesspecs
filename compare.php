<!DOCTYPE html>
<html lang="en">

<head>
	<title>Mobile Planet</title>

	<!-- Meta -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	<!-- Favicon -->
	<link rel="shortcut icon" href="favicon.ico">

	<!-- Web Fonts -->
	<link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,800&amp;subset=cyrillic,latin'>

	<!-- CSS Global Compulsory -->
	<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/shop.style.css">

	<!-- CSS Header and Footer -->
	<link rel="stylesheet" href="assets/css/headers/header-v5.css">
	<link rel="stylesheet" href="assets/css/footers/footer-v4.css">

	<!-- CSS Implementing Plugins -->
	<link rel="stylesheet" href="assets/plugins/animate.css">
	<link rel="stylesheet" href="assets/plugins/line-icons/line-icons.css">
	<link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/plugins/scrollbar/css/jquery.mCustomScrollbar.css">
	<link rel="stylesheet" href="assets/plugins/owl-carousel/owl-carousel/owl.carousel.css">
	<link rel="stylesheet" href="assets/plugins/revolution-slider/rs-plugin/css/settings.css">

	<!-- CSS Theme -->
	<link rel="stylesheet" href="assets/css/theme-colors/default.css" id="style_color">
	<link rel="stylesheet" href="assets/plugins/ladda-buttons/css/custom-lada-btn.css">
	<link rel="stylesheet" href="assets/plugins/hover-effects/css/custom-hover-effects.css">
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
	<!-- CSS Customization -->
	<link rel="stylesheet" href="assets/css/custom.css">
<style type="text/css">
.price {
	font-weight: 700 !important;
	color: #999;
	font-size: 16px;
	width: 220px !important;
	border: 2px solid #999;
	border-radius: 4px;
}
.price:hover {
	color: darkred;
	border: 2px solid green;
	border-radius: 8px;
}
</style>
</head>

<body class="header-fixed">

	<div class="wrapper">

	<?php include("header.php"); ?>

	<?php include("sidebar.php"); ?>

	<div class="col-md-9">
		<h2>Compare mobile phones</h2>
		<div class="row">

<table style="margin-left: 0 !important; padding-left: 0 !important;" class="table table-condensed" id="specs">	    

	<tr>    
    <form action="http://work.mxscripts.com/compare/" method="post">
    
    <td align="left" width="33%">
     
        <label>Product 1</label><br />
        <select name="product1" id="product1" class="form-control">
        	<option>Select Mobile</option>
        	<?php include("connect.php"); 
        	$result = mysqli_query($con, "SELECT * FROM mobiles");
        	while($row=mysqli_fetch_array($result)){
        	$mobile_name = $row['dev_name']; ?>
        		<option value="<?php echo $mobile_name; ?>"><?php echo $mobile_name; ?></option>
        	<?php
        	} 
        	?>
        	
        </select>
        <div class="suggestions_compare1"></div>
        </td> 
    <td align="left" width="33%">
     
        <label>Product 2</label><br />
        <select name="product2" id="product2" class="form-control">
        	<option>Select Mobile</option>
        	<?php include("connect.php"); 
        	$result = mysqli_query($con, "SELECT * FROM mobiles");
        	while($row=mysqli_fetch_array($result)){
        	$mobile_name = $row['dev_name']; ?>
        		<option value="<?php echo $mobile_name; ?>"><?php echo $mobile_name; ?></option>
        	<?php
        	} 
        	?>
        	
        </select> 
        <div class="suggestions_compare2"></div>
        </td>
    <td align="left" width="33%">
     
        <label>Product 3</label><br />
        <select name="product3" id="product3" class="form-control">
        	<option>Select Mobile</option>
        	<?php include("connect.php"); 
        	$result = mysqli_query($con, "SELECT * FROM mobiles");
        	while($row=mysqli_fetch_array($result)){
        	$mobile_name = $row['dev_name']; ?>
        		<option value="<?php echo $mobile_name; ?>"><?php echo $mobile_name; ?></option>
        	<?php
        	} 
        	?>
        	
        </select>
        <div class="suggestions_compare3"></div>

        </td>    
    </form>  
    </tr>
    
    <tr>
      <td>&nbsp;</td>
      <td>
       
      </td>
      <td>
            </td>
      <td>
           
      </td>
    </tr>



	



	    
    
    
	    
</table>

</div>
	</div>

	</div>

	<?php include("footer.php"); ?>
</div><!--/wrapper-->

<!-- JS Global Compulsory -->
<script src="assets/plugins/jquery/jquery.min.js"></script>
<script src="assets/plugins/jquery/jquery-migrate.min.js"></script>
<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/plugins/noUiSlider/jquery.nouislider.all.min.js"></script>
<!-- JS Implementing Plugins -->
<script src="assets/plugins/back-to-top.js"></script>
<script src="assets/plugins/smoothScroll.js"></script>
<script src="assets/plugins/jquery.parallax.js"></script>
<script src="assets/plugins/owl-carousel/owl-carousel/owl.carousel.js"></script>
<script src="assets/plugins/scrollbar/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="assets/plugins/revolution-slider/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script src="assets/plugins/revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script src="assets/plugins/sky-forms-pro/skyforms/js/jquery-ui.min.js"></script>
<!-- JS Customization -->
<script src="assets/js/custom.js"></script>
<!-- JS Page Level -->
<script src="assets/js/shop.app.js"></script>
<script src="assets/js/plugins/owl-carousel.js"></script>
<script src="assets/js/plugins/revolution-slider.js"></script>
<script src="assets/js/plugins/style-switcher.js"></script>
<script type="text/javascript" src="assets/js/plugins/ladda-buttons.js"></script>
<script type="text/javascript" src="js/fonoapi.jquery.min.js"></script>

<script type="text/javascript">

$('#product1').on('change', function() {

	// set token globally
	//$.fn.fonoApi.options.token = "xxx";
	var devname = $("#product1").val();

	$('.suggestions_compare1').fonoApi({
		token : "93a632ad6ffa667fa0ecdc0f5f42ba3b9ce337e25c510acb",
		device : $("#product1").val(),
		limit : 1,
		template : function() {

			// argument contains the data object // *returns html content
			return $.map(arguments, function(obj, i) {

				content  = '<h3>'+ obj.DeviceName + '</h3>';
				content += '<table class="table table-striped" style="width:100%">';
				content += '<tr><th>info</th><th>Description</th></tr>';
				
				for(var key in obj){
				  content += "<tr><td>" + key + "</td><td>" + obj[key] + "</td><tr>";
				}

				content += "</table>";
				return $('<div class="table-responsive"></div>').append(content);
			});

		}
	});
});
$('#product2').on('change', function() {

	// set token globally
	//$.fn.fonoApi.options.token = "xxx";
	var devname = $("#product2").val();

	$('.suggestions_compare2').fonoApi({
		token : "93a632ad6ffa667fa0ecdc0f5f42ba3b9ce337e25c510acb",
		device : $("#product2").val(),
		limit : 1,
		template : function() {

			// argument contains the data object // *returns html content
			return $.map(arguments, function(obj, i) {

				content  = '<h3>'+ obj.DeviceName + '</h3>';
				content += '<table class="table table-striped" style="width:100%">';
				content += '<tr><th>info</th><th>Description</th></tr>';
				
				for(var key in obj){
				  content += "<tr><td>" + key + "</td><td>" + obj[key] + "</td><tr>";
				}

				content += "</table>";
				return $('<div class="table-responsive"></div>').append(content);
			});

		}
	});
});
$('#product3').on('change', function() {

	// set token globally
	//$.fn.fonoApi.options.token = "xxx";
	var devname = $("#product3").val();

	$('.suggestions_compare3').fonoApi({
		token : "93a632ad6ffa667fa0ecdc0f5f42ba3b9ce337e25c510acb",
		device : $("#product3").val(),
		limit : 1,
		template : function() {

			// argument contains the data object // *returns html content
			return $.map(arguments, function(obj, i) {

				content  = '<h3>'+ obj.DeviceName + '</h3>';
				content += '<table class="table table-striped" style="width:100%">';
				content += '<tr><th>info</th><th>Description</th></tr>';
				
				for(var key in obj){
				  content += "<tr><td>" + key + "</td><td>" + obj[key] + "</td><tr>";
				}

				content += "</table>";
				return $('<div class="table-responsive"></div>').append(content);
			});

		}
	});
});
</script>
<script>
	jQuery(document).ready(function() {
		App.init();
		App.initScrollBar();
		App.initParallaxBg();
		OwlCarousel.initOwlCarousel();
		RevolutionSlider.initRSfullWidth();
		//StyleSwitcher.initStyleSwitcher();
	});
</script>

</body>

</html>
