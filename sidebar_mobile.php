<div class="row">
	<div class="col-lg-3 col-md-3 col-sm-3 filter-by-block md-margin-bottom-60">
	<h1 style="background-color: #7587b1 !important;border-radius: 8px;color: white;text-shadow: 2px 2px 4px black;font-size: 28px;border-bottom: 1px solid;border-bottom-color: tomato;padding: 8px;box-shadow: 7px 4px 11px -1px #dedede;">Mobile Finder</h1>
 					<div class="panel-group" id="accordion">
 						<div class="panel panel-default">
 							<!-- <div style="background-color: #708090 !important;" class="panel-heading">
 								<h2 class="panel-title">
 									<a style="color: white;" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
 										Mobile Brands
 										<i class="fa fa-angle-down"></i>
 									</a>
 								</h2>
 							</div> -->
 							<div id="collapseOne" class="panel-collapse collapse in">
 								<div style="text-align: center;" class="panel-body main_sidebar_gradient">
 									<ul class="list-unstyled checkbox-list">
 										<li style="margin-top: 0 !important; margin-bottom: 0 !important;">
 											<a href="apple-mobile-prices.php" class="pulse-shrink brands" target="_apple-mobile-prices.php">Apple</a>
 										</li>
 										<li style="margin-top: 0 !important; margin-bottom: 0 !important;">
 											<a  href="samsung-mobile-prices.php" target="_samsung-mobile-prices.php" class="pulse-shrink brands">Samsung</a>
 										</li style="margin-top: 0 !important; margin-bottom: 0 !important;">
 										<li style="margin-top: 0 !important; margin-bottom: 0 !important;">
 											<a  href="htc-mobile-prices.php" target="_htc-mobile-prices.php" class="pulse-shrink brands">HTC</a>
 										</li>
 										<li style="margin-top: 0 !important; margin-bottom: 0 !important;">
 											<a  href="lg-mobile-prices.php" target="_lg-mobile-prices.php" class="pulse-shrink brands">LG</a>
 										</li>
 										<li style="margin-top: 0 !important; margin-bottom: 0 !important;">
 											<a  href="sony-mobile-prices.php" target="_sony-mobile-prices.php" class="pulse-shrink brands">Sony</a>
 										</li>
 										<li style="margin-top: 0 !important; margin-bottom: 0 !important;">
 											<a  href="qmobile-mobile-prices.php" target="_qmobile-mobile-prices.php" class="pulse-shrink brands">QMobile</a>
 										</li>
 										<li style="margin-top: 0 !important; margin-bottom: 0 !important;">
 											<a  href="huawei-mobile-prices.php" target="_huawei-mobile-prices.php" class="pulse-shrink brands">Huawei</a>
 										</li>
 										<li style="margin-top: 0 !important; margin-bottom: 0 !important;">
 											<a  href="mobile-prices-in-pakistan.php" class="pulse-shrink brands">All Brands</a>
 										</li>
 									</ul>
 								</div>
 							</div>
 						</div>
 					</div><!--/end panel group-->

 					<div class="panel-group" id="accordion-v2">
 						<div class="panel panel-default">
 							<div style="background-color: #7587b1 !important;border-radius: 8px;font-size: 28px;border-bottom: 1px solid;border-bottom-color: tomato;padding: 3px;box-shadow: 7px 4px 11px -1px #dedede; text-align: center;" class="panel-heading">
 								<h2 class="panel-title">
 									<a data-toggle="collapse" data-parent="#accordion-v2" href="#collapseTwo">
 										Search By Price
 										<i class="fa fa-angle-down"></i>
 									</a>
 								</h2>
 							</div>
 							<div id="collapseTwo" class="panel-collapse collapse in">
 								<div style="text-align: center; " class="panel-body main_sidebar_gradient">
 									<ul class="list-unstyled checkbox-list">
 										<li style="width: 100% !important;">
											<form style="width: 100% !important;" action="search_by_price.php" method="POST">
											<label style="width: 100% !important;" class="select">
											<select style="width: 100% !important;" name="price" class="price">
												<option value="0" selected="" disabled="">Choose Mobile</option>
												<option value="iphone">iPhone</option>
												<option value="samsung">Samsung</option>
												<option value="htc">HTC</option>
												<option value="lg">LG</option>
												<option value="huawei">Huawei</option>
												<option value="qmobile">QMobile</option>
												<option value="sony">Sony</option>
											</select>
											<i></i>
											</label>
										</li>
 										<li style="width: 100% !important;">
											<label style="width: 100% !important;" class="select">
											<select style="width: 100% !important;" name="brand" class="price">
												<option value="0" selected="" disabled="">Choose Price</option>
												<option value="a">0 - 5,000</option>
												<option value="b">5,001 - 10,000</option>
												<option value="c">10,000 - 15,000</option>
												<option value="d">15,001 - 20,000</option>
												<option value="e">20,001 - 25,000</option>
												<option value="f">25,001 - 30,000</option>
												<option value="g">Above 30,000</option>
											</select>
											<i></i>
										</label>
										</li>
										<li>
 											<input style="width: 100% !important;" type="submit" name="submit" class="btn-u rounded-2x btn-u-lg">
 										</form>
 									</ul>
 								</div>
 							</div>
 						</div>
 					</div><!--/end panel group-->

 					<div class="panel-group" id="accordion-v4">
 						<div class="panel panel-default">
 							<div style="background-color: #7587b1 !important;border-radius: 8px;font-size: 28px;border-bottom: 1px solid;border-bottom-color: tomato;padding: 3px;box-shadow: 7px 4px 11px -1px #dedede;" class="panel-heading">
 								<h2 class="panel-title">
 									<a href="compare.php" data-toggle="collapse" data-parent="#accordion-v4" href="#collapseFour">
 										Brand Comparison
 										<i class="fa fa-angle-down"></i>
 									</a>
 								</h2>
 							</div>
 							<div id="collapseFour" class="panel-collapse collapse in">
 								<div style="text-align: center;" class="panel-body main_sidebar_gradient">
 									<ul class="list-unstyled checkbox-list">
 										<li>
 											<a style="width: 100% !important;" href="../compare.php" class="btn-u rounded-2x btn-u-lg">Compare</a>
 										</li>
 									</ul>
 								</div>
 							</div>
 						</div>
 					</div>

 					<div class="panel-group" id="accordion-v5">
 						<div class="panel panel-default">
 							<div style="background-color: #7587b1 !important;border-radius: 8px;font-size: 28px;border-bottom: 1px solid;border-bottom-color: tomato;padding: 3px;box-shadow: 7px 4px 11px -1px #dedede;" class="panel-heading">
 								<h2 class="panel-title">
 									<a style="color: white;" data-toggle="collapse" data-parent="#accordion-v5" href="#collapseFive">
 										Latest Additions
 										<i class="fa fa-angle-down"></i>
 									</a>
 								</h2>
 							</div>
 							<div id="collapseFive" class="panel-collapse collapse in">
 								<div style="text-align: left !important;" class="panel-body main_sidebar_gradient">
 									<ul class="list-unstyled checkbox-list">
 									<?php include("connect.php");
 									$mobile_name_splited = substr($mobile_name, 0, strpos($mobile_name, " "));
			 						$result = mysqli_query($con, "SELECT * FROM mobiles WHERE dev_name LIKE '$mobile_name_splited%' ORDER BY id DESC LIMIT 5");
			 						while ($row = mysqli_fetch_array($result)) {
			 							$url = $row['url'];
			 							$mobile_name = $row['dev_name'];
			 							$price = $row['price'];
			 							$image = $row['picture'];
			 							?>
 										<li><a href="<?php echo "../$url";?>"><img src="<?php echo "../$image"; ?>" width="50px" height="50px" alt="<?php echo $mobile_name; ?>"><span style="margin-left: 20px; color: #000; font-size: 12px; line-height: 25px; font-family: 'Open Sans', sans-serif;"><?php echo $mobile_name; ?></span></a>
 										</li>
 									<?php
 									}
 									?>
 									</ul>
 								</div>
 							</div>
 						</div>
 					</div>

 					<div class="panel-group" id="accordion-v6">
 						<div class="panel panel-default">
 							<div style="background-color: #7587b1 !important;border-radius: 8px;font-size: 28px;border-bottom: 1px solid;border-bottom-color: tomato;padding: 3px;box-shadow: 7px 4px 11px -1px #dedede;" class="panel-heading">
 								<h2 class="panel-title">
 									<a data-toggle="collapse" data-parent="#accordion-v6" href="#collapseSix">
 										Top Rated Smartphones
 										<i class="fa fa-angle-down"></i>
 									</a>
 								</h2>
 							</div>
 							<div id="collapseSix" class="panel-collapse collapse in">
 								<div style="text-align: left !important;" class="panel-body main_sidebar_gradient">
 									<ul class="list-unstyled checkbox-list">
 									<?php include("connect.php");
 									
			 						$result = mysqli_query($con, "SELECT * FROM mobiles WHERE price > '50000' LIMIT 6");
			 						while ($row = mysqli_fetch_array($result)) {
			 							$url = $row['url'];
			 							$mobile_name = $row['dev_name'];
			 							$price = $row['price'];
			 							$image = $row['picture'];
			 							?>
 										<li><a href="<?php echo "../$url";?>"><img src="<?php echo "../$image"; ?>" width="50px" height="50px" alt="<?php echo $mobile_name; ?>"><span style="margin-left: 20px; color: #000; font-size: 12px; line-height: 25px; font-family: 'Open Sans', sans-serif;"><?php echo $mobile_name; ?></span></a>
 										</li>
 									<?php
 									}
 									?>
 									</ul>
 								</div>
 							</div>
 						</div>
 					</div>

 					<div class="panel-group margin-bottom-30" id="accordion-v7">
 						<div class="panel panel-default">
 							<div style="background-color: #7587b1 !important;border-radius: 8px;font-size: 28px;border-bottom: 1px solid;border-bottom-color: tomato;padding: 3px;box-shadow: 7px 4px 11px -1px #dedede;" class="panel-heading">
 								<h2 class="panel-title">
 									<a data-toggle="collapse" data-parent="#accordion-v7" href="#collapseSeven">
 										Latest Smartphones
 										<i class="fa fa-angle-down"></i>
 									</a>
 								</h2>
 							</div>
 							<div id="collapseSeven" class="panel-collapse collapse in">
 								<div style="text-align: left !important;" class="panel-body main_sidebar_gradient">
 									<ul class="list-unstyled checkbox-list">
 									<?php include("connect.php");
 									
			 						$result = mysqli_query($con, "SELECT * FROM mobiles ORDER BY id ASC LIMIT 5");
			 						while ($row = mysqli_fetch_array($result)) {
			 							$url = $row['url'];
			 							$mobile_name = $row['dev_name'];
			 							$price = $row['price'];
			 							$image = $row['picture'];
			 							?>
 										<li><a href="<?php echo "../$url";?>"><img src="<?php echo "../$image"; ?>" width="50px" height="50px" alt="<?php echo $mobile_name; ?>"><span style="margin-left: 20px; color: #000; font-size: 12px; line-height: 25px; font-family: 'Open Sans', sans-serif;"><?php echo $mobile_name; ?></span></a>
 										</li>
 									<?php
 									}
 									?>
 									</ul>
 								</div>
 							</div>
 						</div>
 					</div>
 				</div>
