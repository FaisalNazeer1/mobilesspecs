		<!--=== Header ===-->
		<div style="background-color: #333 !important;" class="header-v5">
			<!-- Topbar -->
			<div class="topbar-v3">
				<div class="search-open">
					<div class="container">
						<input type="text" class="form-control" placeholder="Search">
						<div class="search-close"><i class="icon-close"></i></div>
					</div>
				</div>

				<div class="container">
					<div class="row">
						<div class="col-sm-6">
							<!-- Topbar Navigation -->
							<ul class="left-topbar">
								<li>
									<a style="color: #fff !important;">Currency (RS)</a>
									<ul class="currency">
										<li class="active">
											<a href="#">RS <i class="fa fa-check"></i></a>
										</li>
										<!-- <li><a href="#">Euro</a></li>
										<li><a href="#">Pound</a></li> -->
									</ul>
								</li>
								<li>
									<a style="color: #fff !important;">Language (EN)</a>
									<ul class="language">
										<li class="active">
											<a href="#">English (EN)<i class="fa fa-check"></i></a>
										</li>
										<!-- <li><a href="#">Spanish (SPN)</a></li>
										<li><a href="#">Russian (RUS)</a></li>
										<li><a href="#">German (GRM)</a></li> -->
									</ul>
								</li>
							</ul><!--/end left-topbar-->
						</div>
						<div class="col-sm-6">
							<ul class="list-inline right-topbar pull-right">
								<!-- <li><a href="#">Account</a></li>
								<li><a href="shop-ui-add-to-cart.html">Wishlist (0)</a></li> -->
								<li><a style="color: #fff !important;" href="login.php">Login</a> | <a style="color: #fff !important;" href="register.php">Register</a></li>
								<!-- <li><i class="search fa fa-search search-button"></i></li> -->
							</ul>
						</div>
					</div>
				</div><!--/container-->
			</div>
			<!-- End Topbar v3 -->

			<!-- Navbar -->
				<div style="margin-bottom: 30px;" class="col-lg-12 col-md-12 col-sm-12">
					<div id="logo" style="margin-left: 0 !important; margin-top: 0 !important; padding-left: 0 !important; float: left !important;" class="col-lg-3 col-md-3 col-sm-3">
						<div style="z-index: 11" class="col-md-4">
							<a class="navbar-brand" href="../index.php">
								<img id="logo-header" width="180px" src="../img/mpnew2.png" alt="Logo">
							</a>
						</div>
					</div>
						
						<div style="margin-top: 30px;" class="col-lg-4 col-md-4 col-sm-12 col-lg-offset-1">
							<form id="searchform">
								<div style="width: 100%;" class="input-group searchForm">
									<input style="width: 100%;" type="text" class="form-control searchForm" size="30" value="" id="inputString" onkeyup="lookup(this.value);" placeholder="Search Here..." autocomplete="off">
									<!-- <span class="input-group-btn">
										<button style="background-color: #6694ae; border: 1px solid; z-index: 1; margle" class="btn-u" type="button"><i class="fa fa-search fa-2x"></i></button>
									</span> -->
								</div>
								<div id="suggestions"></div>
							</form>
							</div>
								<div id="social-icons" style="margin-top: 23px; z-index: 2 !important;" class="col-lg-3 pull-right">
									<div class="input-group">
											<a href="" rel="hover" style="width: 44px; height: 42px; z-index: 2 !important;" class="btn hover hoverfb rounded-x margin-right-5 margin-left-5 margin-bottom-5"><i class="fa fa-facebook fa-2x" style="color: #3b5998;"></i></a>
											<a href="" style="width: 44px; height: 42px; z-index: 2;" rel="hover" class="btn  hover hovergp rounded-x margin-right-5 margin-left-5"><i class="fa fa-google fa-2x" style="color: #d34836;"></i></a>
											<a href="" style="width: 44px; height: 42px; z-index: 2;" rel="hover" class="btn hover hovertw rounded-x margin-right-5 margin-left-5"><i class="fa fa-twitter fa-2x" style="color: skyblue;"></i></a>
											<a href="" style="width: 44px; height: 42px; z-index: 2;" rel="hover" class="btn  hover hoveryt rounded-x margin-right-5 margin-left-5"><i class="fa fa-youtube fa-2x" style="color: red;"></i></a>
									</div>
								</div>
					</div>
						

					<!-- Shopping Cart -->
					<div style="display: none;" class="shop-badge badge-icons pull-right">
						<a href="#"><i class="fa fa-shopping-cart"></i></a>
						<span class="badge badge-sea rounded-x">3</span>
						<div class="badge-open">
							<ul class="list-unstyled mCustomScrollbar" data-mcs-theme="minimal-dark">
								<li>
									<img src="assets/img/thumb/05.jpg" alt="">
									<button type="button" class="close">×</button>
									<div class="overflow-h">
										<span>Black Glasses</span>
										<small>1 x $400.00</small>
									</div>
								</li>
								<li>
									<img src="assets/img/thumb/02.jpg" alt="">
									<button type="button" class="close">×</button>
									<div class="overflow-h">
										<span>Black Glasses</span>
										<small>1 x $400.00</small>
									</div>
								</li>
								<li>
									<img src="assets/img/thumb/03.jpg" alt="">
									<button type="button" class="close">×</button>
									<div class="overflow-h">
										<span>Black Glasses</span>
										<small>1 x $400.00</small>
									</div>
								</li>
							</ul>
							<div class="subtotal">
								<div class="overflow-h margin-bottom-10">
									<span>Subtotal</span>
									<span class="pull-right subtotal-cost">$1200.00</span>
								</div>
								<div class="row">
									<div class="col-xs-6">
										<a href="shop-ui-inner.html" class="btn-u btn-brd btn-brd-hover btn-u-sea-shop btn-block">View Cart</a>
									</div>
									<div class="col-xs-6">
										<a href="shop-ui-add-to-cart.html" class="btn-u btn-u-sea-shop btn-block">Checkout</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- End Shopping Cart -->

					<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="navbar navbar-default mega-menu" role="navigation" style="border-radius:0px">
				<div style="margin: 0 auto !important;" class="row">
					<div class="col-xs-12 col-sm-12">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div style="background-color: white !important; width: 100% !important; margin: 0 auto !important;" class="navbar-header">
						<button style="margin: 0 auto !important; float: none;" type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span>MENU</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>
					</div>
				</div>
				<div class="container">
					<div class="collapse navbar-collapse navbar-responsive-collapse">
						<!-- Nav Menu -->
						<ul class="nav navbar-nav">
							<!-- Home -->
							<li class="dropdown">
								<a style="border: 0 !important;" href="../index.php" class="dropdown-toggle" data-hover="dropdown">
									Home
								</a>
							</li>
							<!-- End Home -->

							<!-- Promotion -->
							<li class="dropdown">
								<a href="../mobile_brands.php?brand_name=all_brands" style="border: 0 !important;" class="dropdown-toggle" data-hover="dropdown">
									Mobile Brands
								</a>
							</li>
							<!-- End Mobile brands -->

							<!-- Reviews -->
							<li class="dropdown">
								<a href="javascript:void(0);" class="dropdown-toggle" style="border: 0 !important;" data-hover="dropdown" >
									Reviews
								</a>
							</li>
							<!-- End Reviews -->

							<!-- Blog -->
							<li class="dropdown">
								<a href="javascript:void(0);" class="dropdown-toggle" style="border: 0 !important;" data-hover="dropdown" >
									Blog
								</a>
							</li>
							<!-- End Blog -->

							<!-- Outlet -->
							<!-- <li class="dropdown" style="margin-top: -38px;">
								<a href="javascript:void(0);" class="dropdown-toggle" data-hover="dropdown">
									Outlet
								</a>
							</li> -->
							<!-- End Outlet -->

							<!-- Main Others -->
							<li class="dropdown"><a class="dropdown-toggle" style="border: 0 !important;" href="../coverage.php">Coverage</a></li>
							<li class="dropdown"><a class="dropdown-toggle" style="border: 0 !important;" href="../contact_us.php">Contact</a></li>
							<!-- Main Others -->
						</ul>
						<!-- End Nav Menu -->
					</div>
				</div>
			</div>
			<!-- End Navbar -->
		</div>
		<!--=== End Header ===-->