<!--=== Footer v4 ===-->
	<div class="footer-v4">
		<div class="footer">
			<div style="margin-left: 6%; margin-right: 6%;" class="container">
				<div class="row">
					<!-- About -->
					<div class="col-md-6 md-margin-bottom-40">
						<a href="index.html"><img class="footer-logo" width="250px" src="img/mpnew.png" alt=""></a>
						<br>
						<ul class="list-unstyled address-list margin-bottom-20">
							<li><i class="fa fa-angle-right"></i>Abbottabad, Pakistan, KPK IT PARK</li>
							<li><i class="fa fa-angle-right"></i>Phone: 800 123 3456</li>
							<li><i class="fa fa-angle-right"></i>Fax: 800 123 3456</li>
							<li><i class="fa fa-angle-right"></i>Email: info@mobilesspecs.com</li>
						</ul>
					</div>
					<!-- End About -->

					<!-- Simple List -->
					<div class="col-md-3 col-sm-3">
						<div class="row">

							<div class="col-sm-12 col-xs-6">
								<h2 class="thumb-headline">Mobile Brands</h2>
								<ul class="list-unstyled simple-list">
									<li><a href="mobile_brands=Apple">Apple</a></li>
									<li><a href="mobile_brands=Samsung"Samsung></a></li>
									<li><a href="mobile_brands=Htc">HTC </a></li>
									<li><a href="mobile_brands=Lg">LG</a></li>
									<li>...</li>
								</ul>
							</div>

							<div class="col-sm-12 col-xs-6">
								<h2 class="thumb-headline">Recent Mobiles</h2>
								<ul class="list-unstyled simple-list">
									<li><a href="#">Samsung Galaxy S7</a></li>
									<li><a href="#">iPhone 6s</a></li>
									<li><a href="#">HTC one M </a></li>
									<!-- <li><a href="#">Francisco papa</a></li> -->
									<li>...</li>
								</ul>
							</div>
						</div>
					</div>

					<div class="col-md-3 col-sm-3">
						<div class="row">
							<div class="col-sm-12 col-xs-6">
								<h2 class="thumb-headline">Outlets</h2>
								<ul class="list-unstyled simple-list margin-bottom-20">
									<li><a href="#">Abbottabad</a></li>
									<li><a href="#">Islamabad</a></li>
									<li><a href="#">Lahore</a></li>
									<li><a href="#">Peshawar </a></li>
									<li>...</li>
								</ul>
							</div>

							<div class="col-sm-12 col-xs-6">
								<h2 class="thumb-headline">About Us</h2>
								<ul class="list-unstyled simple-list margin-bottom-20">
									<!-- <li><a href="#">Ultima Hora</a></li>
									<li><a href="#">Exclusiva</a></li>
									<li>...</li> -->
								</ul>
							</div>

							<div class="col-sm-12 col-xs-6">
								<h2 class="thumb-headline">Contact Us</h2>
								<ul class="list-unstyled simple-list">
									<!-- <li><a href="#">Ultima Hora</a></li>
									<li><a href="#">Exclusiva</a></li>
									<li>...</li> -->
								</ul>
							</div>
							
						</div>
					</div>

					<!-- End Simple List -->
				</div><!--/end row-->
			</div><!--/end continer-->
		</div><!--/footer-->

		<div class="copyright">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<p>
							2017 &copy; MOBILESSPECS.COM ALL Rights Reserved.
							<a target="_https://www.mobilesspecs.com" href="https://www.mobilesspecs.com">Mobiles Specs</a> | <a href="#">Privacy Policy</a> | <a href="#">Terms of Service</a>
						</p>
					</div>
					<div class="col-md-6">
						<ul class="list-inline shop-social pull-left">
							<li><h2 class="thumb-headline">Join us on</h2></li>
						</ul>
						<ul class="list-inline shop-social pull-right">
							<li><a href="#"><i onmouseover="this.style.backgroundColor='darkblue'" onmouseout="this.style.backgroundColor='#414040'" class="fb rounded-4x fa fa-facebook"></i></a></li>
							<li><a href="#"><i onmouseover="this.style.backgroundColor='blue'" onmouseout="this.style.backgroundColor='#414040'" class="tw rounded-4x fa fa-twitter"></i></a></li>
							<li><a href="#"><i onmouseover="this.style.backgroundColor='red'" onmouseout="this.style.backgroundColor='#414040'" class="gp rounded-4x fa fa-google-plus"></i></a></li>
							<li><a href="#"><i onmouseover="this.style.backgroundColor='darkred'" onmouseout="this.style.backgroundColor='#414040'" class="yt rounded-4x fa fa-youtube"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div><!--/copyright-->
	</div>
	<!--=== End Footer v4 ===