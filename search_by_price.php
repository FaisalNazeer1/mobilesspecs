<!DOCTYPE html>
<html lang="en">

<head>
	<title>Mobile Planet</title>

	<!-- Meta -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	<!-- Favicon -->
	<link rel="shortcut icon" href="favicon.ico">

	<!-- Web Fonts -->
	<link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,800&amp;subset=cyrillic,latin'>

	<!-- CSS Global Compulsory -->
	<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/shop.style.css">

	<!-- CSS Header and Footer -->
	<link rel="stylesheet" href="assets/css/headers/header-v5.css">
	<link rel="stylesheet" href="assets/css/footers/footer-v4.css">

	<!-- CSS Implementing Plugins -->
	<link rel="stylesheet" href="assets/plugins/animate.css">
	<link rel="stylesheet" href="assets/plugins/line-icons/line-icons.css">
	<link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/plugins/scrollbar/css/jquery.mCustomScrollbar.css">
	<link rel="stylesheet" href="assets/plugins/owl-carousel/owl-carousel/owl.carousel.css">
	<link rel="stylesheet" href="assets/plugins/revolution-slider/rs-plugin/css/settings.css">

	<!-- CSS Theme -->
	<link rel="stylesheet" href="assets/css/theme-colors/default.css" id="style_color">
	<link rel="stylesheet" href="assets/plugins/ladda-buttons/css/custom-lada-btn.css">
	<link rel="stylesheet" href="assets/plugins/hover-effects/css/custom-hover-effects.css">
  <link rel="stylesheet" href="search_css/style.css">
  <script type="text/javascript" src="http://www.google.com/jsapi"></script>
  <script type="text/javascript" src="search_js/script.js"></script>
	
	<!-- CSS Customization -->
	<link rel="stylesheet" href="assets/css/custom.css">
<style type="text/css">
/*Breadcrumbs v4
------------------------------------*/
.breadcrumbs-v4 {
  width: 100%;
  padding: 60px 0;
  position: relative;
  background-position: top left;
  background-repeat: no-repeat;
  background-image: url(smartphones/androidphones3.png);
  background-size:cover;
}

.breadcrumbs-v4:before {
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  content: " ";
  position: absolute;
  background: rgba(0,0,0,0.3);
}

.breadcrumbs-v4 .container {
  position: relative;
}

.breadcrumbs-v4 span.page-name {
  color: #fff;
  display: block;
  font-size: 18px;
  font-weight: 200;
  margin: 0 0 5px 3px;
}

.breadcrumbs-v4 h1 {
  color: #fff;
  font-size: 40px;
  font-weight: 200;
  margin: 0 0 20px;
  line-height: 50px;
  text-transform: uppercase;
}

.breadcrumbs-v4 .breadcrumb-v4-in {
  padding-left: 0;
  margin-bottom: 0;
  list-style: none;
}

.breadcrumbs-v4 .breadcrumb-v4-in > li {
  color: #fff;
  font-size: 13px;
  display: inline-block;
}

.breadcrumbs-v4 .breadcrumb-v4-in > li + li:before {
  color: #fff;
  content: "\f105";
  margin-left: 7px;
  padding-right: 8px;
  font-family: FontAwesome;
}

.breadcrumbs-v4 .breadcrumb-v4-in li a {
  color: #fff;
}

.breadcrumbs-v4 .breadcrumb-v4-in li.active,
.breadcrumbs-v4 .breadcrumb-v4-in li a:hover {
  color: #18ba9b;
  text-decoration: none;
}

@media (max-width: 768px) {
  .breadcrumbs-v4 {
    text-align: center;
  }

  .breadcrumbs-v4 span.page-name {
    font-size: 18px;
  }

  .breadcrumbs-v4 h1 {
    font-size: 30px;
    margin-bottom: 10px;
  }
}
#mobile_overview::-webkit-scrollbar { 
    display: none; 
}
#mobile_overview {
    height: 100%;
}

object {
    width: 100%;
    min-height: 100%;
}
li > a {
	color: white !important; font-weight: 600;
}
.content_image_main_gradient {
background:-webkit-linear-gradient(239deg, rgba(255,0,0,1) 0%, rgba(255,146,69,1) 14%, rgba(255,92,239,1) 31%, rgba(5,193,255,1) 79%, rgba(0,0,0,1) 99%, rgba(0,0,0,1) 100%);
background:-o-linear-gradient(239deg, rgba(255,0,0,1) 0%, rgba(255,146,69,1) 14%, rgba(255,92,239,1) 31%, rgba(5,193,255,1) 79%, rgba(0,0,0,1) 99%, rgba(0,0,0,1) 100%);
background:-moz-linear-gradient(239deg, rgba(255,0,0,1) 0%, rgba(255,146,69,1) 14%, rgba(255,92,239,1) 31%, rgba(5,193,255,1) 79%, rgba(0,0,0,1) 99%, rgba(0,0,0,1) 100%);
background:linear-gradient(239deg, rgba(255,0,0,1) 0%, rgba(255,146,69,1) 14%, rgba(255,92,239,1) 31%, rgba(5,193,255,1) 79%, rgba(0,0,0,1) 99%, rgba(0,0,0,1) 100%);
  background-size: contain;
  background-repeat: no-repeat;
  background-position: center;
}

.container .content {
  padding-right: 5px !important; padding-left: 5px !important;
  margin-right: 5px !important; margin-left: 5px !important;
}
.content-md {
  padding-left: 20px;
}
.img-responsive {
	width: 160px !important;
	height: auto;
	margin: 0 auto;
	padding-top: 20px;
	padding-bottom: 20px;
}
.product-img:hover .img-responsive {
  -webkit-transform: rotateY(180deg);
-webkit-transform-style: preserve-3d;
transform: rotateY(180deg);
transform-style: preserve-3d;
}
.product-img .img-responsive, .product-img:hover .img-responsive {
-webkit-transition: all 0.7s ease;
transition: all 0.7s ease;
}
.illustration-v2 .add-to-cart {
  left: 0;
  right: 0;
  top: 50%;
  z-index: 1;
  width: 100%;
  border: none;
  padding: 10px 0;
  font-size: 12px !important;
  margin-top: -20px;
  text-align: center;
  position: absolute;
  visibility: hidden;
  text-transform: uppercase;
  color: white;
  background: url(img/bg_pattern2.png) repeat;
  text-shadow: 1px 1px 2px rgba(0,0,0,.8);
}
.illustration-v2 .add-to-cart:hover {
  color: #fff;
  text-decoration: none;
  background: rgba(0,0,0, 0.8) !important;
}
.main_sidebar_gradient {
  background: #EBEBEB;
  background-size: contain;
  background-repeat: no-repeat;
  background-position: center;
}
li > a {
  color: white !important; font-weight: 600;
  font-family: 'Open Sans', sans-serif !important;
}
.brands {
  color: #4F4F4F !important;
}
.brands:hover {
  color: black !important;
  font-size: 24px !important;
  width: 100% !important;
  background: rgba(255,255,255,0.6);
}
.badge-results {
    color: #fff !important;
    font-size: inherit;
    position: relative;
}
.shop-bg-red {
    background: #c9253c;
}
</style>
</head>

<body class="header-fixed">

	<div class="wrapper">
  <?php
  include("connect.php");
  $mobile_brand = $_POST['brand'];
  $price_range = $_POST['price'];
  include("connect.php");
 	if ($price_range > 30000) {
 		$result = mysqli_query($con, "SELECT * FROM mobiles WHERE price > '$price_range'");
 	}else{
 		$result = mysqli_query($con, "SELECT * FROM mobiles WHERE price < '$price_range' AND > '$price_range' - 4999");
 	}
 	$number_of_result = mysqli_num_rows($result);
  ?>
	<?php include("header.php"); ?>
	<!--=== Breadcrumbs v4 ===-->
 		<div class="breadcrumbs-v4">
 			<div class="container">
 				<!-- <span class="page-name">Product Filter Page</span> -->
 				<h1><?php echo "SEARCH BY PRICE"; ?> <span class="shop-red"><strong></strong></span></h1>
 				<!-- <ul class="breadcrumb-v4-in">
 					<li><a href="index.html">Home</a></li>
 					<li><a href="#">Product</a></li>
 					<li class="active">Product Filter Page</li>
 				</ul> -->
 			</div><!--/end container-->
 		</div>
 		<!--=== End Breadcrumbs v4 ===-->
    <div class="content-md">
          <h1><span class="shop-red">MOBILE BRAND:</span> <strong><?php echo mb_strtoupper($mobile_brand); ?></strong></h1>
          <span class="shop-bg-red badge-results"><?php echo "<strong>( $number_of_result )</strong>"; ?> smartphone(s) found for your desired options</span>
          <hr>
 			<div class="row illustration-v2 margin-bottom-30">

 			<?php 
            
            while ($row = mysqli_fetch_array($result)) {
              $url = $row['url'];
              $mobile_name = $row['dev_name'];
              $price = $row['price'];
              $image = $row['picture'];
              ?>
              <div style="margin-top: 30px;" class="col-md-2 col-xs-2 col-sm-2">
                <div class="product-img product-img-brd">
                  <a href="<?php echo $url; echo "?m_name=$mobile_name"; ?>"><img class="full-width img-responsive" src="<?php echo $image; ?>" alt="<?php echo $mobile_name; ?>"></a>
                  <a class="product-review" href="<?php echo $url; echo "?m_name=$mobile_name"; ?>"><?php echo $mobile_name; ?></a>
                  <a class="add-to-cart" href="<?php echo $url; echo "?m_name=$mobile_name"; ?>"><!-- <i class="fa fa-shopping-cart"></i> -->PRICE RS <?php echo $price; ?></a>
                </div>
              </div>
            <?php
            }
            ?>

 		</div>
 		<hr>
  </div>
	<?php include("footer.php"); ?>
  </div><!--/end wrapper-->
<!-- JS Global Compulsory -->
<script src="assets/plugins/jquery/jquery.min.js"></script>
<script src="assets/plugins/jquery/jquery-migrate.min.js"></script>
<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/plugins/noUiSlider/jquery.nouislider.all.min.js"></script>
<!-- JS Implementing Plugins -->
<script src="assets/plugins/back-to-top.js"></script>
<script src="assets/plugins/smoothScroll.js"></script>
<script src="assets/plugins/jquery.parallax.js"></script>
<script src="assets/plugins/owl-carousel/owl-carousel/owl.carousel.js"></script>
<script src="assets/plugins/scrollbar/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="assets/plugins/revolution-slider/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script src="assets/plugins/revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script src="assets/plugins/sky-forms-pro/skyforms/js/jquery-ui.min.js"></script>
<!-- JS Customization -->
<script src="assets/js/custom.js"></script>
<!-- JS Page Level -->
<script src="assets/js/shop.app.js"></script>
<script src="assets/js/plugins/owl-carousel.js"></script>
<script src="assets/js/plugins/revolution-slider.js"></script>
<script src="assets/js/plugins/style-switcher.js"></script>
<script type="text/javascript" src="assets/js/plugins/ladda-buttons.js"></script>
<script>
	jQuery(document).ready(function() {
		App.init();
		App.initScrollBar();
		App.initParallaxBg();
		OwlCarousel.initOwlCarousel();
		RevolutionSlider.initRSfullWidth();
		//StyleSwitcher.initStyleSwitcher();
	});
</script>

</body>

</html>
