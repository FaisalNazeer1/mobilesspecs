<!DOCTYPE html>
<html>
<head>
  <title></title>
<link rel="stylesheet" type="text/css" href="gsmarena.css">
  <link rel="stylesheet" type="text/css" href="specs.css">
</head>
<body>

<div class="main main-review right l-box col">  

<div class="review-header hreview">  
    <div class="article-info">
<style type="text/css">
    .review-header::after {
	background-image:url( http://cdn2.gsmarena.com/vv/bigpic/samsung-galaxy-s8-.jpg );
	background-size: 970%;
	background-position: -3218px -7637px;
    }
</style>


        <div class="article-info-line page-specs light border-bottom">
            <div class="blur review-background"></div>
            <h1 class="specs-phone-name-title">Samsung Galaxy S8</h1>



<script>
	var sURLSocialE = "http%3A%2F%2Fwww.gsmarena.com%2Fsamsung_galaxy_s8-8161.php";
</script>


<ul class="social-share sharrre">
<li class="help help-social">
  <a class="box btnFb" title="Facebook" id="facebook" href="http://www.facebook.com/sharer.php?u=http%3A%2F%2Fwww.gsmarena.com%2Fsamsung_galaxy_s8-8161.php" target="_blank" onclick="window.open('http://www.facebook.com/sharer.php?u=http%3A%2F%2Fwww.gsmarena.com%2Fsamsung_galaxy_s8-8161.php','fbookshare','width=500,height=400,left='+(screen.availWidth/2-225)+',top='+(screen.availHeight/2-150)+'');return false;">
    
    <div class="share">
      <span class="count fbCount" href="#">-</span>
      <i class="head-icon icon-soc-fb2"></i>
    </div>
  </a>
</li>
<li>
  <a class="box btnTw" title="Twitter" id="twitter" href="https://twitter.com/intent/tweet?text=Samsung+Galaxy+S8&amp;url=http%3A%2F%2Fwww.gsmarena.com%2Fsamsung_galaxy_s8-8161.php" target="_blank" onclick="window.open('https://twitter.com/intent/tweet?text=Samsung+Galaxy+S8&amp;url=http%3A%2F%2Fwww.gsmarena.com%2Fsamsung_galaxy_s8-8161.php','twitshare','width=500,height=400,left='+(screen.availWidth/2-225)+',top='+(screen.availHeight/2-150)+'');return false;">
    <div class="share">
      <span class="count twCount" href="#">-</span>
      <i class="head-icon icon-soc-twitter2"></i></div>
  </a>
</li>
<li>
  <a class="box btnGo" title="Google+" id="googleplus" href="https://plusone.google.com/_/+1/confirm?hl=en&amp;url=http%3A%2F%2Fwww.gsmarena.com%2Fsamsung_galaxy_s8-8161.php" target="_blank" onclick="window.open('https://plus.google.com/share?url=http%3A%2F%2Fwww.gsmarena.com%2Fsamsung_galaxy_s8-8161.php','gplusshare','width=500,height=400,left='+(screen.availWidth/2-225)+',top='+(screen.availHeight/2-150)+'');return false;">
    
    <div class="share">
      <span class="count goCount" href="#">-</span>
      <i class="head-icon icon-soc-gplus"></i>
    </div>
  </a>
</li>
</ul>             


        </div>




        

<div class="center-stage light nobg specs-accent">
  <div class="specs-photo-main">
<a href="samsung_galaxy_s8-pictures-8161.php"><img alt="Samsung Galaxy S8
MORE PICTURES" src="http://cdn2.gsmarena.com/vv/bigpic/samsung-galaxy-s8-.jpg"></a>      
  </div>
  <ul class="specs-spotlight-features" style="overflow:hidden;">
    <li class="specs-brief pattern">
      <span class="specs-brief-accent"><i class="head-icon icon-launched"></i>Released 2017, April</span><br>
      <span class="specs-brief-accent"><i class="head-icon icon-mobile2"></i>155g, 8mm thickness</span><br>
      <span class="specs-brief-accent"><i class="head-icon icon-os"></i>Android OS, v7.0</span><br>
      <span class="specs-brief-accent"><i class="head-icon icon-sd-card-0"></i>64GB storage, microSD card slot</span>  
      
      <span class="help help-quickfacts"></span>
    </li>
    
    <li class="light pattern help help-popularity">
     
      <strong class="accent"> <i class="head-icon icon-popularity"></i>94%</strong>
      <span>9,402,494 hits</span></li>
    <li class="light pattern help help-fans">
      <a class="specs-fans" href="">
        <strong class="accent"><i class="head-icon icon-fans"></i>947</strong>
        <span>Become a fan</span>
      </a>
    </li>
    <li class="help accented help-display">
      <i class="head-icon icon-touch-1"></i>
      <strong class="accent">5.8"</strong>1440x2960 pixels    </li>
    <li class="help accented help-camera">
      <i class="head-icon icon-camera-1"></i>
      <strong class="accent accent-camera">12<span>MP</span>
      </strong> 2160p</li>
    <li class="help accented help-expansion">
      <i class="head-icon icon-cpu"></i>
      <strong class="accent accent-expansion">4<span>GB RAM</span></strong> 
      Exynos 8895 Octa</li>


    <li class="help accented help-battery">
    <i class="head-icon icon-battery-1"></i>
    
    <strong class="accent accent-battery">3000<span>mAh</span></strong>Li-Ion</li>
    
  </ul>
</div>

<div class="article-info-line page-specs light">

<ul class="article-info-meta">

<li class="article-info-meta-link article-info-meta-link-review light large help help-review"><a href="samsung_galaxy_s8_s8plus_hands_on-review-1595.php"><i class="head-icon icon-review"></i>Preview</a></li><li class="article-info-meta-link light"><a href="samsung_galaxy_s8-3d-spin-8161.php"><i class="head-icon icon-360-view"></i>360° view</a></li>

<li class="article-info-meta-link light"><a href="samsung_galaxy_s8-pictures-8161.php"><i class="head-icon icon-pictures"></i>Pictures</a></li>
<li class="article-info-meta-link light"><a href="compare.php3?idPhone1=8161"><i class="head-icon icon-mobile-phone231 icon-compare"></i>Compare</a></li>
<li class="article-info-meta-link light" title="Samsung Galaxy S8 user reviews and opinions"><a href="samsung_galaxy_s8-reviews-8161.php"><i class="head-icon icon-comment-count"></i>Opinions</a></li>


<br style="clear:both;">
</ul>
</div>

</div> 			
</div>


<script language="JavaScript">
<!--
function helpW( strURL )
{
window.open( 'help.php3?term=' + strURL, '_blank', 'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=420,height=390' );
}
//-->
</script>

<div id="specs-list">
					
<style type="text/css"> .tr-toggle {  display:none; }  </style>


<table cellspacing="0">
<tbody><tr class="tr-hover">
<th rowspan="15" scope="row">Network</th>
<td class="ttl"><a href="network-bands.php3">Technology</a></td>
<td class="nfo"><a href="#" class="link-network-detail">GSM / HSPA / LTE</a></td>
</tr>
<tr class="tr-toggle" style="display: table-row;">
<td class="ttl"><a href="network-bands.php3">2G bands</a></td>
<td class="nfo">GSM 850 / 900 / 1800 / 1900 - SIM 1 &amp; SIM 2 (dual-SIM model only)</td>
</tr><tr class="tr-toggle" style="display: table-row;">
<td class="ttl"><a href="network-bands.php3">3G bands</a></td>
<td class="nfo">HSDPA 850 / 900 / 1700(AWS) / 1900 / 2100 </td>
</tr>
<tr class="tr-toggle" style="display: table-row;">
<td class="ttl"><a href="network-bands.php3">4G bands</a></td>
<td class="nfo">LTE band 1(2100), 2(1900), 3(1800), 4(1700/2100), 5(850), 7(2600), 8(900), 17(700), 20(800), 28(700)</td>
</tr>
<tr class="tr-toggle" style="display: table-row;">
<td class="ttl"><a href="glossary.php3?term=3g">Speed</a></td>
<td class="nfo">HSPA 42.2/5.76 Mbps, LTE-A (4CA) Cat16 1024/150 Mbps</td>
</tr>
	
<tr class="tr-toggle" style="display: table-row;">
<td class="ttl"><a href="glossary.php3?term=gprs">GPRS</a></td>
<td class="nfo">Yes</td>
</tr>	
<tr class="tr-toggle" style="display: table-row;">
<td class="ttl"><a href="glossary.php3?term=edge">EDGE</a></td>
<td class="nfo">Yes</td>
</tr>
</tbody></table>


<table cellspacing="0">
<tbody><tr>
<th rowspan="2" scope="row">Launch</th>
<td class="ttl"><a href="#" onclick="helpW('h_year.htm');">Announced</a></td>
<td class="nfo">2017, March</td>
</tr>	
<tr>
<td class="ttl"><a href="#" onclick="helpW('h_status.htm');">Status</a></td>
<td class="nfo">Available. Released 2017, April</td>
</tr>
</tbody></table>


<table cellspacing="0">
<tbody><tr>
<th rowspan="6" scope="row">Body</th>
<td class="ttl"><a href="#" onclick="helpW('h_dimens.htm');">Dimensions</a></td>
<td class="nfo">148.9 x 68.1 x 8 mm (5.86 x 2.68 x 0.31 in)</td>
</tr><tr>
<td class="ttl"><a href="#" onclick="helpW('h_weight.htm');">Weight</a></td>
<td class="nfo">155 g (5.47 oz)</td>
</tr>
<tr>
<td class="ttl"><a href="glossary.php3?term=build">Build</a></td>
<td class="nfo">Corning Gorilla Glass 5 back panel</td>
</tr>
<tr>
<td class="ttl"><a href="glossary.php3?term=sim">SIM</a></td>
<td class="nfo">Single SIM (Nano-SIM) or Dual SIM (Nano-SIM, dual stand-by)</td>
</tr>
<tr><td class="ttl">&nbsp;</td><td class="nfo">- Samsung Pay (Visa, MasterCard certified)<br>
- IP68 certified - dust/water proof over 1.5 meter and 30 minutes</td></tr>
		
</tbody></table>


<table cellspacing="0">
<tbody><tr>
<th rowspan="6" scope="row">Display</th>
<td class="ttl"><a href="glossary.php3?term=display-type">Type</a></td>
<td class="nfo">Super AMOLED capacitive touchscreen, 16M colors</td>
</tr>
<tr>
<td class="ttl"><a href="#" onclick="helpW('h_dsize.htm');">Size</a></td>
<td class="nfo">5.8 inches (~83.6% screen-to-body ratio)</td>
</tr>
<tr>
<td class="ttl"><a href="#" onclick="helpW('h_dres.htm');">Resolution</a></td>
<td class="nfo">1440 x 2960 pixels (~570 ppi pixel density)</td>
</tr>
<tr>
<td class="ttl"><a href="glossary.php3?term=multitouch">Multitouch</a></td>
<td class="nfo">Yes</td>
</tr>
<tr>
<td class="ttl"><a href="glossary.php3?term=screen-protection">Protection</a></td>
<td class="nfo">Corning Gorilla Glass 5</td>
</tr>
<tr><td class="ttl">&nbsp;</td><td class="nfo">- 3D Touch (home button only)<br>
- Always-on display<br>
- TouchWiz UI</td></tr>
		
</tbody></table>


<table cellspacing="0">
<tbody><tr>
<th rowspan="4" scope="row">Platform</th>
<td class="ttl"><a href="glossary.php3?term=os">OS</a></td>
<td class="nfo">Android OS, v7.0</td>
</tr>
<tr><td class="ttl"><a href="glossary.php3?term=chipset">Chipset</a></td>
<td class="nfo">Exynos 8895 Octa - EMEA<br>Qualcomm MSM8998 Snapdragon 835 - US model</td>
</tr>
<tr><td class="ttl"><a href="glossary.php3?term=cpu">CPU</a></td>
<td class="nfo">Octa-core (4x2.3 GHz &amp; 4x1.7 GHz) - EMEA<br>Octa-core (4x2.35 GHz Kryo &amp; 4x1.9 GHz Kryo) - US model</td>
</tr>
<tr><td class="ttl"><a href="glossary.php3?term=gpu">GPU</a></td>
<td class="nfo">Mali-G71 MP20 - EMEA<br>Adreno 540 - US model</td>
</tr>
</tbody></table>


<table cellspacing="0">
<tbody><tr>
<th rowspan="5" scope="row">Memory</th>
<td class="ttl"><a href="glossary.php3?term=memory-card-slot">Card slot</a></td>


<td class="nfo">microSD, up to 256 GB (dedicated slot) - single-SIM model<br>microSD, up to 256 GB (uses SIM 2 slot) - dual-SIM model</td></tr>

	

<tr>
<td class="ttl"><a href="glossary.php3?term=dynamic-memory">Internal</a></td>
<td class="nfo">64 GB, 4 GB RAM</td>
</tr>
	

			


</tbody></table>


<table cellspacing="0">
<tbody><tr>
<th rowspan="4" scope="row">Camera</th>
<td class="ttl"><a href="glossary.php3?term=camera">Primary</a></td>
<td class="nfo">12 MP, f/1.7, 26mm, phase detection autofocus, OIS, LED flash, <a href="piccmp.php3?idType=1&amp;idPhone1=8161&amp;nSuggest=1">check quality</a></td>
</tr>
<tr>
<td class="ttl"><a href="glossary.php3?term=camera">Features</a></td>
<td class="nfo">1/2.5" sensor size, 1.4 µm pixel size, geo-tagging, simultaneous 4K video and 9MP image recording, touch focus, face/smile detection, Auto HDR, panorama</td>
</tr>
<tr>
<td class="ttl"><a href="glossary.php3?term=camera">Video</a></td>
<td class="nfo">2160p@30fps, 1080p@60fps, HDR, dual-video rec., <a href="vidcmp.php3?idType=3&amp;idPhone1=8161&amp;nSuggest=1">check quality</a></td>
</tr>
<tr>
<td class="ttl"><a href="glossary.php3?term=video-call">Secondary</a></td>
<td class="nfo">8 MP, f/1.7, autofocus, 1440p@30fps, dual video call, Auto HDR</td>
</tr>
</tbody></table>


<table cellspacing="0">
<tbody><tr>
<th rowspan="4" scope="row">Sound</th>
<td class="ttl"><a href="glossary.php3?term=call-alerts">Alert types</a></td>
<td class="nfo">Vibration; MP3, WAV ringtones</td>
	</tr>
	
<tr>
<td class="ttl"><a href="glossary.php3?term=loudspeaker">Loudspeaker</a> </td>
<td class="nfo">Yes</td>
</tr>

<tr>
<td class="ttl"><a href="glossary.php3?term=audio-jack">3.5mm jack</a> </td>
<td class="nfo">Yes</td>
</tr>
	

<tr><td class="ttl">&nbsp;</td><td class="nfo">- 32-bit/384kHz audio<br>
- Active noise cancellation with dedicated mic</td></tr>
	
</tbody></table>


<table cellspacing="0">
<tbody><tr>
<th rowspan="9" scope="row">Comms</th>
<td class="ttl"><a href="glossary.php3?term=wi-fi">WLAN</a></td>
<td class="nfo">Wi-Fi 802.11 a/b/g/n/ac, dual-band, Wi-Fi Direct, hotspot</td>
</tr>
<tr>
<td class="ttl"><a href="glossary.php3?term=bluetooth">Bluetooth</a></td>
<td class="nfo">v5.0, A2DP, LE, aptX</td>
</tr>
<tr>
<td class="ttl"><a href="glossary.php3?term=gps">GPS</a></td>
<td class="nfo">Yes, with A-GPS, GLONASS, BDS, GALILEO</td>
</tr>  
<tr>
<td class="ttl"><a href="glossary.php3?term=nfc">NFC</a></td>
<td class="nfo">Yes</td>
</tr>
	
	
<tr>
<td class="ttl"><a href="glossary.php3?term=fm-radio">Radio</a></td>
<td class="nfo">No</td>
</tr>
   
<tr>
<td class="ttl"><a href="glossary.php3?term=usb">USB</a></td>
<td class="nfo">v3.1, Type-C 1.0 reversible connector</td>
</tr>
</tbody></table>


<table cellspacing="0">
<tbody><tr>
<th rowspan="12" scope="row">Features</th>
<td class="ttl"><a href="glossary.php3?term=sensors">Sensors</a></td>
<td class="nfo">Iris scanner, fingerprint (rear-mounted), accelerometer, gyro, proximity, compass, barometer, heart rate, SpO2</td>
</tr><tr>
<td class="ttl"><a href="glossary.php3?term=messaging">Messaging</a></td>
<td class="nfo">SMS(threaded view), MMS, Email, Push Mail, IM</td>
</tr><tr>
<td class="ttl"><a href="glossary.php3?term=browser">Browser</a></td>
<td class="nfo">HTML5</td>
</tr> 	

 	
<tr>
<td class="ttl"><a href="glossary.php3?term=java">Java</a></td>
<td class="nfo">No</td>
</tr>
<tr><td class="ttl">&nbsp;</td><td class="nfo">- Samsung DeX (desktop experience support)<br>
- Fast battery charging<br>
- Qi/PMA wireless charging (market dependent)<br>
- ANT+ support<br>
- S-Voice natural language commands and dictation<br>
- MP4/DivX/XviD/H.265 player<br>
- MP3/WAV/eAAC+/FLAC player<br>
- Photo/video editor<br>
- Document editor</td></tr>
	
</tbody></table>


<table cellspacing="0">
<tbody><tr>
<th rowspan="7" scope="row">Battery</th>
<td class="ttl">&nbsp;</td>
<td class="nfo">Non-removable Li-Ion 3000 mAh battery</td>
</tr>


</tbody></table>


<table cellspacing="0">
<tbody><tr>
<th rowspan="5" scope="row">Misc</th>
<td class="ttl"><a href="#" onclick="helpW('h_colors.htm');">Colors</a></td>
<td class="nfo">Midnight Black, Orchid Gray, Arctic Silver, Coral Blue, Maple Gold</td>
</tr>
<tr>
<td class="ttl"><a href="glossary.php3?term=sar">SAR EU</a></td>
<td class="nfo">
0.32 W/kg (head) &nbsp; &nbsp; 1.27 W/kg (body) &nbsp; &nbsp; </td>
</tr>


<tr>
<td class="ttl"><a href="#" onclick="helpW('h_price.htm');">Price group</a></td>
<td class="nfo"><a href="#">9/10</a> <span class="price">(About 800 EUR)</span></td>
</tr>
</tbody></table>


<table cellspacing="0">
<tbody><tr>
<th rowspan="1" scope="row">Tests</th>


<td class="ttl"><a href="gsmarena_lab_tests-review-751p5.php">Camera</a></td>
<td class="nfo">
<a class="noUnd" href="piccmp.php3?idType=1&amp;idPhone1=8161&amp;nSuggest=1">Photo</a> / <a class="noUnd" href="vidcmp.php3?idType=3&amp;idPhone1=8161&amp;nSuggest=1">Video</a></td>
</tr><tr>

</tr></tbody></table>

</div>

</div>
</div>