<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

<head>
	<title>Mobiles Specs – Mobile Phone Prices in Pakistan</title>

	<!-- Meta -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="Mobiles Specs is the Pakistan’s largest portal for Mobile Prices in Pakistan. Check the latest mobile phone prices in Pakistan and compare mobile phones.">
	<meta name="author" content="">

	<!-- Favicon -->
	<link rel="shortcut icon" href="favicon.ico">

	<!-- Web Fonts -->
	<link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,800&amp;subset=cyrillic,latin'>

	<!-- CSS Global Compulsory -->
	<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/shop.style.css">

	<!-- CSS Header and Footer -->
	<link rel="stylesheet" href="assets/css/headers/header-v5.css">
	<link rel="stylesheet" href="assets/css/footers/footer-v4.css">

	<!-- CSS Implementing Plugins -->
	<link rel="stylesheet" href="assets/plugins/animate.css">
	<link rel="stylesheet" href="assets/plugins/line-icons/line-icons.css">
	<link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/plugins/scrollbar/css/jquery.mCustomScrollbar.css">
	<link rel="stylesheet" href="assets/plugins/owl-carousel/owl-carousel/owl.carousel.css">
	<link rel="stylesheet" href="assets/plugins/revolution-slider/rs-plugin/css/settings.css">

	<!-- CSS Theme -->
	<link rel="stylesheet" href="assets/css/theme-colors/default.css" id="style_color">
	<link rel="stylesheet" href="assets/plugins/ladda-buttons/css/custom-lada-btn.css">
	<link rel="stylesheet" href="assets/plugins/hover-effects/css/custom-hover-effects.css">
	<link rel="stylesheet" href="assets/plugins/hover-effects/css/hover.css">
	<link rel="stylesheet" href="search_css/style.css">
	<script type="text/javascript" src="http://www.google.com/jsapi"></script>
	<script type="text/javascript" src="search_js/script.js"></script>
	<!-- CSS Customization -->
	<!-- <link rel="stylesheet" href="assets/css/custom.css"> -->
<style type="text/css">
/*html { -moz-transform: scale(0.8, 0.8); zoom: 0.8; zoom: 80%; }*/
.price {
	font-weight: 700 !important;
	color: #999;
	font-size: 16px;
	width: 200px !important;
	height: 40px;
	border: 2px solid #999;
	border-radius: 4px;
	padding-left: 10px;
}
.price:hover {
	color: black;
	border: 2px solid black;
	border-radius: 8px;
}
li > a {
	color: rgba(49, 49, 49, 0.73) !important;
	font-family: 'Open Sans', sans-serif !important;
}
.brands {
	color: #4F4F4F !important;
	font-size: 24px !important;
	font-weight: 600;
	text-decoration: none; 
	margin-left: 0 !important;
}
.brands:hover {
	color: black !important;
	font-size: 24px !important;
	width: 100% !important;
	background: rgba(255,255,255,0.6);
}
.maincontainer {
	margin-top: 5px;
	overflow: hidden;
	background-color: white;
	width: 83.5%;
	height: 350px;
	float: left;
}
.contentcontainer {
	background-color: #FFC0CB;
	overflow: hidden;
	width: 100%;
	position: relative;
	height: 350px;
}
.contentcontainer:hover {
	background-color: #FFC0CB;
	overflow: hidden;
	width: 100%;
	position: relative;
	height: 350px;
}
.content_image_main {
	width: 60%;
	float: left;
	height: 350px;
	border: 1px dotted;
	border-color: white;
	z-index: 0;
	background-size: 100% 100%;
	background-repeat: no-repeat;
	background-position: center;
}
@media screen and (min-width: 350px) and (max-width: 760px){
	.maincontainer {
		width: 100% !important;
		margin-left: 8px;
	}
}

.content_image_main_gradient {
  background-color: #6694ae;
  background-size: contain;
  background-repeat: no-repeat;
  background-position: center;
}

.main_sidebar_gradient {
  background: rgba(225, 223, 255, 0.36) !important;
  background-size: contain;
  background-repeat: no-repeat;
  background-position: center;
}
.price2 {
	visibility: hidden;
}
.second_image {
	opacity: 0;
}
.content_image_main:hover {
	background: rgba(255,80,180,0.8);
	background-size: 110% 110%;
	background-repeat: repeat;
	background-position: center;
	-webkit-transition: all 0.7s ease;
	transition: all 0.7s ease;
}
.content_image_main:hover .price2{
	visibility: visible;
	left: 35% !important;
    right: 0;
    margin-top: 190px;
    margin-bottom: 20px;
    margin-left: 35% !important;
    width: 30%;
    color: #fff;
    border: none;
    padding: 10px 0;
    font-size: 20px;
    text-align: center;
    text-transform: uppercase;
    /*background: rgba(255,255,255,0.8);*/
    background: url(img/bg_pattern2.png) repeat;
    text-shadow: 1px 1px 2px rgba(0,0,0,.8);
    -webkit-transition: all 0.7s ease;
	transition: all 0.7s ease;
}
.price3 {
	visibility: hidden;
}
.content_image_main:hover .price3{
	visibility: visible;
	left: 25% !important;
    right: 0;
    margin-top: 100px;
    margin-bottom: 20px;
    margin-left: 25% !important;
    width: 50%;
    color: #fff;
    border: none;
    padding: 10px 0;
    font-size: 20px;
    text-align: center;
    text-transform: uppercase;
    /*background: rgba(255,255,255,0.8);*/
    background: url(img/bg_pattern2.png) repeat;
    text-shadow: 1px 1px 2px rgba(0,0,0,.8);
    -webkit-transition: all 0.7s ease;
	transition: all 0.7s ease;
}
.content_image:hover .price3{
	visibility: visible;
	left: 35%;
    right: 0;
    margin-top: 30px;
    margin-left: 26%;
    width: 50%;
    color: #fff;
    border: none;
    padding: 10px 0;
    font-size: 16px;
    text-align: center;
    text-transform: uppercase;
    background: url(img/bg_pattern2.png) repeat;
    text-shadow: 1px 1px 2px rgba(0,0,0,.8);
    -webkit-transition: all 0.7s ease;
}
.price3:hover {
	color: white !important;
	background: rgba(0, 0, 0, 0.6) !important;
}
/*.content_image_main > img:hover .price2 {
	visibility: visible;
	left: 0;
    right: 0;
    margin-top: 180px;
    width: 100%;
    color: #555;
    border: none;
    padding: 10px 0;
    font-size: 20px;
    text-align: center;
    text-transform: uppercase;
    background: rgba(255,255,255,0.8);
}*/

.content_image {
	width: 40%;
	float: left;
	height: 175px;
	border: 1px dotted;
	border-color: white;
	z-index: 0;
	background-size: 100% 100%;
	background-repeat: no-repeat;
	background-position: center;
}
.content_image:hover .price2{
	visibility: visible;
	left: 35%;
    right: 0;
    margin-top: 40px;
    margin-left: 38%;
    width: 25%;
    color: #fff;
    border: none;
    padding: 10px 0;
    font-size: 20px;
    text-align: center;
    text-transform: uppercase;
    background: url(img/bg_pattern2.png) repeat;
    text-shadow: 1px 1px 2px rgba(0,0,0,.8);
    -webkit-transition: all 0.7s ease;
}
.content_image:hover {
	background: rgba(202,82,182,0.8);
	background-size: 110% 110%;
	background-repeat: repeat;
	-webkit-transition: all 0.4s ease-in;
	transition: all 0.4s ease-in;
}
.price2:hover {
	color: white !important;
	background: rgba(0, 0, 0, 0.6) !important;
}

.mobile_name {
	width: 30%;
	text-align: center;
	margin-left: 10px;
	margin-top: -10px !important;
}
.mobile_name h3 {
  font: 400 25px/1.28 "Google-Oswald", Arimo, Arial, sans-serif;
  color: white;
  background: url(img/bg_pattern2.png) repeat;
  text-shadow: 1px 1px 2px rgba(0,0,0,.8);
  /*text-shadow: 2px 2px 3px black;*/
  text-transform: uppercase; }
.mobile_name:hover h3 {
	color: white !important;
	background: rgba(0, 0, 0, 0.6) !important;
}
.mobile_name h2 {
  font: 400 16px/1.28 "Google-Oswald", Arimo, Arial, sans-serif;
  color: white;
  background: url(img/bg_pattern2.png) repeat;
  text-shadow: 1px 1px 2px rgba(0,0,0,.8);
  text-transform: uppercase; }
.mobile_name:hover h2 {
	color: white !important;
	background: rgba(0, 0, 0, 0.2) !important;
}
.secondary_container {
	margin-top: 5px;
	overflow: hidden;
	background-color: white;
	width: 75%;
	height: 1784px;
	float: left;
	border: 1px solid black;
}
.secondary_container_content1 {
	width: 25%;
	float: left;
	border: 1px solid black;
	height: auto;
}
.secondary_container_content2 {
	width: 25%;
	float: left;
	border: 1px solid black;
	height: auto;
}
.secondary_container_content3 {
	width: 25%;
	float: left;
	border: 1px solid black;
	height: auto;
}

@media screen and (min-width: 350px) and (max-width: 760px){
	.secondary_container_content1 {
	width: 33%;
	float: left;
	border: 1px solid black;
	height: auto;
	margin-left: 8px;
}
	.secondary_container_content2 {
	width: 32%;
	float: left;
	border: 1px solid black;
	height: auto;
	margin-left: 5px;
	
}
	.secondary_container_content3 {
	width: 32%;
	border: 1px solid black;
	height: auto;
	margin-left: 3px;
}
}
.product-review {
	text-decoration: none;
	font-size: 11px !important;
	font-weight: 600 !important;
	text-shadow: 1px 1px 1px rgba(0,0,0,.8);
	margin-top: -40px !important;
	height: 38px;
}
.product-review:hover {
	font-size: 11px !important;
	color: yellow !important;
	text-decoration: none;
	font-weight: 600 !important;
	text-shadow: 1px 1px 1px rgba(0,0,0,.8);
}
.product-img:hover .img-hover {
	-webkit-transform: rotateY(180deg);
-webkit-transform-style: preserve-3d;
transform: rotateY(180deg);
transform-style: preserve-3d;
}
.product-img .img-hover, .product-img:hover .img-hover {
-webkit-transition: all 0.7s ease;
transition: all 0.7s ease;
}
.illustration-v2 .add-to-cart {
	left: 0;
	right: 0;
	top: 60%;
	z-index: 1;
	width: 165% !important;
	border: none;
	padding: 10px 0;
	font-size: 12px !important;
	margin-top: -20px;
	margin-left: -40px;
	text-align: center;
	position: absolute;
	visibility: hidden;
	text-transform: uppercase;
	color: white;
  	background: url(img/bg_pattern2.png) repeat;
  	text-shadow: 1px 1px 2px rgba(0,0,0,.8);
}
.illustration-v2 .add-to-cart:hover {
	color: #fff;
	text-decoration: none;
	background: rgba(0,0,0, 0.8) !important;
}
.hover {
	background-color: ghostwhite;
	color: black;
}
.hoverfb {
	color: #3b5998 !important;
}
.hoverfb:hover {
	background-color: #3b5998 !important;
	color: white !important;
}
.hoverfb:hover .fa{
  color: white !important;
}
.hovergp {
	color: #d34836;
}
.hovergp:hover {
	background-color: #d34836 !important;
	color: white !important;
}
.hovergp:hover .fa{
	color: white !important;
}

.hovertw {
	color: skyblue;
}
.hovertw:hover{
	background-color: skyblue !important;
}
.hovertw:hover .fa{
	color: white !important;
}
.hoveryt {
	color: white;
}
.hoveryt:hover {
	background-color: red !important;
	color: white !important;
}
.hoveryt:hover .fa{
	color: white !important;
}
/*@keyframes bounce {
	0%, 20%, 60%, 100% {
		-webkit-transform: translateY(0);
		transform: translateY(0);
	}

	40% {
		-webkit-transform: translateY(-12px);
		transform: translateY(-12px);
	}

	80% {
		-webkit-transform: translateY(-6px);
		transform: translateY(-6px);
	}
}*/
.dropdown-toggle {
	font-family: 'Google-Oswald', Arial, sans-serif !important;
}
/*.dropdown-toggle:hover {
	animation: bounce 0.9s;
	color: white !important;
	font-weight: 600 !important;
	font-size: 24px !important;
	scale: ;
	-webkit-transition: all 0.7s ease;
	transition: all 0.7s ease;
}*/
.dropdown {
	margin-top: -38px;
}
.nav {
	color: white !important; margin-top: 0 !important;
}
.sft {
	font-size: 28px !important; margin-top: 10px !important;
}
.rounded-2x {
	font-weight: 600; 
	background-color: #7587b1 !important;
}
.rounded-2x:hover {
	color: black !important;
	background-color: tomato !important;
	font-weight: 700;
}
@media screen and (min-width: 250px) and (max-width: 760px){
	#logo {
		display: none;
	}
	#social-icons {
		display: none;
	}
}
@media screen and (min-width: 350px) and (max-width: 760px){
	#logo {
		display: none;
	}
	#social-icons {
		display: none;
	}
	.dropdown {
		margin-top: 0px;
	}
	.nav {
		color: white !important; margin-top: 0px;
	}
	.sft {
		font-size: 22px !important; margin-top: 90px !important;
	}
	.filter-by-block {
		width: 100% !important;
	}
	.brands {
		margin-left: 10% !important;
	}
	.product-img {
		margin: 0 auto !important;
	}
	.navbar-bra {
		display: none;
	}
	.logo-header {
		display: none;
	}
	.devices_list {
		margin-left: 0px !important;
	}
	.device_box_v2 {
		margin-left: 30px;
	}
	.contentcontainer {
		height: 150px !important;
	}
	.content {
		padding-right: 0 !important;
	}
	.top-margin {
		margin-top: 0 !important;
	}
	.all-smartphones {
		width: 100%;
		height: 100px !important;
		padding-right: 0;
    	padding-top: 10px;
	}
}
.icon-bar {
	width: 200px !important;
}
.devices_list {
    height: 220px;
    border-radius: 15px;
    display: block;
    margin-bottom: 20px;
    text-align: center;
    vertical-align: central;
    overflow: hidden;
    margin-left: 25px;
}
.device_box_v2 {
	padding-top: 15px;
	/*margin-left: 20px;*/
}
.thumb-headline {
	font-size: 24px !important;
}

ul.pagination {
    display: inline-block;
    padding: 0;
    margin: 0;
}

ul.pagination li {display: inline;}

ul.pagination li a {
    color: black !important;
    float: left;
    padding: 8px 16px;
    text-decoration: none;
    transition: background-color .3s;
    border: 1px solid #ddd;
    margin: 0 4px;
}

ul.pagination li a.current {
    background-color: #4CAF50;
    color: white;
    border: 1px solid #4CAF50;
}

ul.pagination li a:hover:not(.current) {background-color: #ddd;}
.row {
	margin-right: 0 !important;
}
.top-margin {
	margin-top: 10px;
}
.all-smartphones {
	width: 100%; 
	height: 200px;
}
.img-responsive {
	display: inline-block;
	height: 155px;
}
.panel-body {
	padding: 0;
}
hr.style13 {
	height: 10px;
	margin-bottom: 0;
}


hr.style14 { 
  border: 0; 
  height: 1px; 
  background-image: -webkit-linear-gradient(left, #f0f0f0, #8c8b8b, #f0f0f0);
  background-image: -moz-linear-gradient(left, #f0f0f0, #8c8b8b, #f0f0f0);
  background-image: -ms-linear-gradient(left, #f0f0f0, #8c8b8b, #f0f0f0);
  background-image: -o-linear-gradient(left, #f0f0f0, #8c8b8b, #f0f0f0); 
}
.panel {
	border-radius: 8px; 
	border-color: white !important;
}
.panel div {
	text-align: center;
}
.panel-collapse {
	//border: 4px solid; 
	border-radius: 8px; 
	border-color: darkgray;
}
.panel-title {
	margin-top: 5px !important;
	margin-bottom: 5px !important;
}
.panel-body {
	border-radius: 5px;
}
.panel-title a {
	color: white !important;text-shadow: 2px 2px 4px black;
}
</style>

</head>

<body class="header-fixed">

	<div class="wrapper">

	<?php include("header.php"); ?>

		<!--=== Slider ===-->
		<div style="height: 400px;" class="tp-banner-container">
			<div style="height: auto !important;" class="tp-banner">
				<ul>
					<!-- SLIDE -->
					<li class="revolution-mch-1" data-transition="fade" data-slotamount="5" data-masterspeed="1000" data-title="Slide 1">
						<!-- MAIN IMAGE -->
						<img src="smartphones/androidphones.png"  alt="darkblurbg"  data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">

						<div style="font-size: 28px !important; margin-top: 10px !important;" class="tp-caption revolution-ch1 sft start"
						data-x="center"
						data-hoffset="0"
						data-y="100"
						data-speed="1500"
						data-start="500"
						data-easing="Back.easeInOut"
						data-endeasing="Power1.easeIn"
						data-endspeed="300">
						All <strong>Smartphones</strong> are
					</div>

					<!-- LAYER -->
					<div class="tp-caption revolution-ch4 sft"
					data-x="center"
			data-hoffset="-14"
			data-y="250"
			data-speed="1400"
			data-start="2000"
			data-easing="Power4.easeOut"
			data-endspeed="300"
			data-endeasing="Power1.easeIn"
			data-captionhidden="off"
					style="z-index: 6">
					<strong style="font-size: 22px !important;">with updated prices</strong>
					<!-- <a href="#" class="btn-u btn-brd btn-brd-hover btn-u-light">Shop Now</a> -->
				</div>
			</li>
			<!-- END SLIDE -->

			<!-- SLIDE -->
			<li class="revolution-mch-1" data-transition="fade" data-slotamount="5" data-masterspeed="1000" data-title="Slide 2">
				<!-- MAIN IMAGE -->
				<img src="smartphones/androidphones2.png"  alt="darkblurbg"  data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">

				<div style="font-size: 28px !important; margin-top: 10px !important;" class="tp-caption revolution-ch3 sft start"
				data-x="center"
				data-hoffset="0"
				data-y="140"
				data-speed="1500"
				data-start="500"
				data-easing="Back.easeInOut"
				data-endeasing="Power1.easeIn"
				data-endspeed="300">
				Latest <strong>Smartphones</strong> Trends
			</div>

			<!-- LAYER -->
			<div class="tp-caption revolution-ch4 sft"
			data-x="center"
			data-hoffset="-14"
			data-y="210"
			data-speed="1400"
			data-start="2000"
			data-easing="Power4.easeOut"
			data-endspeed="300"
			data-endeasing="Power1.easeIn"
			data-captionhidden="off"
			style="z-index: 6; font-size: 20px !important;">
			<br><br>
			<small>The eye of quality measures</small><br><br>
			<small>starts right from here</small>
		</div>

		<!-- LAYER -->
		<div class="tp-caption sft"
		data-x="center"
		data-hoffset="0"
		data-y="300"
		data-speed="1600"
		data-start="1800"
		data-easing="Power4.easeOut"
		data-endspeed="300"
		data-endeasing="Power1.easeIn"
		data-captionhidden="off"
		style="z-index: 6">
		<!-- <a href="#" class="btn-u btn-brd btn-brd-hover btn-u-light">Shop Now</a> -->
	</div>
</li>
<!-- END SLIDE -->

<!-- SLIDE -->
<li class="revolution-mch-1" data-transition="fade" data-slotamount="5" data-masterspeed="1000" data-title="Slide 3">
	<!-- MAIN IMAGE -->
	<img src="smartphones/androidphones33.png"  alt="darkblurbg"  data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">

	<div style="font-size: 28px !important; margin-top: 10px !important;" class="tp-caption revolution-ch3 sft start" 
	data-x="right"
	data-hoffset="5"
	data-y="130"
	data-speed="1500"
	data-start="500"
	data-easing="Back.easeInOut"
	data-endeasing="Power1.easeIn"
	data-endspeed="300">
	<strong>Luxury</strong> Smartphones
</div>

<!-- LAYER -->
  <div class="tp-caption revolution-ch4 sft" 
  data-x="right"
  data-hoffset="0"
  data-y="210"
  data-speed="1400"
  data-start="2000"
  data-easing="Power4.easeOut"
  data-endspeed="300"
  data-endeasing="Power1.easeIn"
  data-captionhidden="off"
  style="z-index: 6">
  </div>

<!-- LAYER -->
  <div class="tp-caption sft"
  data-x="right"
  data-hoffset="0"
  data-y="300"
  data-speed="1600"
  data-start="2800"
  data-easing="Power4.easeOut"
  data-endspeed="300"
  data-endeasing="Power1.easeIn"
  data-captionhidden="off"
  style="z-index: 6">
<!-- <a href="#" class="btn-u btn-brd btn-brd-hover btn-u-light">Shop Now</a> -->
</div>
</li>
<!-- END SLIDE -->

<!-- SLIDE -->
<li class="revolution-mch-1" data-transition="fade" data-slotamount="5" data-masterspeed="1000" data-title="Slide 4">
	<!-- MAIN IMAGE -->
	<img src="smartphones/androidphones4.png"  alt="darkblurbg"  data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">

	<div style="font-size: 28px !important; margin-top: 10px !important;" class="tp-caption revolution-ch1 sft start"
	data-x="center"
	data-hoffset="0"
	data-y="100"
	data-speed="1500"
	data-start="500"
	data-easing="Back.easeInOut"
	data-endeasing="Power1.easeIn"
	data-endspeed="300">
	All <strong>Brands</strong> Available Here
</div>

<!-- LAYER -->
  <div style="font-size: 28px !important; margin-top: 10px !important;" class="tp-caption revolution-ch2 sft"
  data-x="center"
  data-hoffset="0"
  data-y="280"
  data-speed="1400"
  data-start="2000"
  data-easing="Power4.easeOut"
  data-endspeed="300"
  data-endeasing="Power1.easeIn"
  data-captionhidden="off"
  style="z-index: 6">
  With Exact Prices
  </div>

<!-- LAYER -->
  <div class="tp-caption sft"
  data-x="center"
  data-hoffset="0"
  data-y="370"
  data-speed="1600"
  data-start="2800"
  data-easing="Power4.easeOut"
  data-endspeed="300"
  data-endeasing="Power1.easeIn"
  data-captionhidden="off"
  style="z-index: 6">
<!-- <a href="#" class="btn-u btn-brd btn-brd-hover btn-u-light">View More</a>
<a href="#" class="btn-u btn-brd btn-brd-hover btn-u-light">Shop Now</a> -->
</div>
</li>
<!-- END SLIDE -->

<!-- SLIDE -->
<li class="revolution-mch-1" data-transition="fade" data-slotamount="5" data-masterspeed="1000" data-title="Slide 5">
	<!-- MAIN IMAGE -->
	<img src="smartphones/iphones.jpg"  alt="darkblurbg"  data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">

	<div style="font-size: 28px !important; margin-top: 10px !important;" class="tp-caption revolution-ch5 sft start"
	data-x="right"
	data-hoffset="5"
	data-y="130"
	data-speed="1500"
	data-start="500"
	data-easing="Back.easeInOut"
	data-endeasing="Power1.easeIn"
	data-endspeed="300">
	<strong>Superb</strong> Smartphones Collection
</div>

<!-- LAYER -->
  <div class="tp-caption revolution-ch4 sft"
  data-x="right"
  data-hoffset="-14"
  data-y="210"
  data-speed="1400"
  data-start="2000"
  data-easing="Power4.easeOut"
  data-endspeed="300"
  data-endeasing="Power1.easeIn"
  data-captionhidden="off"
  style="z-index: 6">
  </div>

<!-- LAYER -->
  <div class="tp-caption sft"
  data-x="right"
  data-hoffset="0"
  data-y="300"
  data-speed="1600"
  data-start="2800"
  data-easing="Power4.easeOut"
  data-endspeed="300"
  data-endeasing="Power1.easeIn"
  data-captionhidden="off"
  style="z-index: 6">
  <!-- <a href="#" class="btn-u btn-brd btn-brd-hover btn-u-light">Shop Now</a> -->
  </div>
</li>
<!-- END SLIDE -->
</ul>
<div class="tp-bannertimer tp-bottom"></div>
</div>
</div>
                <!--=== End Slider ===-->

	<div style=" width: 100%; padding-right: 30px;" class="content container">
 			<div class="row">
 				<div class="col-lg-3 col-md-3 col-sm-3 filter-by-block md-margin-bottom-60">
 					<h1 style="background-color: #7587b1 !important;border-radius: 8px;color: white;text-shadow: 2px 2px 4px black;font-size: 28px;border-bottom: 1px solid;border-bottom-color: tomato;padding: 8px;box-shadow: 7px 4px 11px -1px #dedede;">Mobile Finder</h1>
 					<div class="panel-group" id="accordion">
 						<div style="" class="panel panel-default">
 							<!-- <div style="background-color: #708090 !important;" class="panel-heading">
 								<h2 class="panel-title">
 									<a style="color: white;" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
 										Mobile Brands
 										<i class="fa fa-angle-down"></i>
 									</a>
 								</h2>
 							</div> -->
 							<div style="" id="collapseOne" class="panel-collapse collapse in">
 								<div style="text-align: center;" class="panel-body main_sidebar_gradient">
 									<ul class="list-unstyled checkbox-list">
 										<li style="margin-top: 0 !important; margin-bottom: 0 !important;">
 											<a href="apple-mobile-prices.php" class="pulse-shrink brands" target="apple-mobile-prices.php">Apple</a>
 										</li>
 										<li style="margin-top: 0 !important; margin-bottom: 0 !important;">
 											<a href="samsung-mobile-prices.php" target="samsung-mobile-prices.php" class="pulse-shrink brands">Samsung</a>
 										</li style="margin-top: 0 !important; margin-bottom: 0 !important;">
 										<li style="margin-top: 0 !important; margin-bottom: 0 !important;">
 											<a href="htc-mobile-prices.php" target="htc-mobile-prices.php" class="pulse-shrink brands">HTC</a>
 										</li>
 										<li style="margin-top: 0 !important; margin-bottom: 0 !important;">
 											<a href="lg-mobile-prices.php" target="lg-mobile-prices.php" class="pulse-shrink brands">LG</a>
 										</li>
 										<li style="margin-top: 0 !important; margin-bottom: 0 !important;">
 											<a href="sony-mobile-prices.php" target="sony-mobile-prices.php" class="pulse-shrink brands">Sony</a>
 										</li>
 										<li style="margin-top: 0 !important; margin-bottom: 0 !important;">
 											<a href="qmobile-mobile-prices.php" target="qmobile-mobile-prices.php" class="pulse-shrink brands">QMobile</a>
 										</li>
 										<li style="margin-top: 0 !important; margin-bottom: 0 !important;">
 											<a href="huawei-mobile-prices.php" target="huawei-mobile-prices.php" class="pulse-shrink brands">Huawei</a>
 										</li>
 										<li style="margin-top: 0 !important; margin-bottom: 0 !important;">
 											<a href="mobile-brands-in-pakistan.php" class="pulse-shrink brands">All Brands</a>
 										</li>
 									</ul>
 								</div>
 							</div>
 						</div>
 					</div><!--/end panel group-->

 					<div class="panel-group" id="accordion-v2">
 						<div class="panel panel-default">
 							<div style="background-color: #7587b1 !important;border-radius: 8px;font-size: 28px;border-bottom: 1px solid;border-bottom-color: tomato;padding: 3px;box-shadow: 7px 4px 11px -1px #dedede;" class="panel-heading">
 								<h2 class="panel-title">
 									<a data-toggle="collapse" data-parent="#accordion-v2" href="#collapseTwo">
 										Search By Price
 										<i class="fa fa-angle-down"></i>
 									</a>
 								</h2>
 							</div>
 							<div id="collapseTwo" class="panel-collapse collapse in">
 								<div style="text-align: center; " class="panel-body main_sidebar_gradient">
 									<ul class="list-unstyled checkbox-list">
 										<li style="width: 100% !important;">
											<form style="width: 100% !important;" action="search_by_price.php" method="POST">
											<label style="width: 100% !important;" class="select">
											<select style="width: 100% !important;" name="price" class="price">
												<option value="0" selected="" disabled="">Choose Mobile</option>
												<option value="iphone">iPhone</option>
												<option value="samsung">Samsung</option>
												<option value="htc">HTC</option>
												<option value="lg">LG</option>
												<option value="huawei">Huawei</option>
												<option value="qmobile">QMobile</option>
												<option value="sony">Sony</option>
											</select>
											<i></i>
											</label>
										</li>
 										<li style="width: 100% !important;">
											<label style="width: 100% !important;" class="select">
											<select style="width: 100% !important;" name="brand" class="price">
												<option value="0" selected="" disabled="">Choose Price</option>
												<option value="a">0 - 5,000</option>
												<option value="b">5,001 - 10,000</option>
												<option value="c">10,000 - 15,000</option>
												<option value="d">15,001 - 20,000</option>
												<option value="e">20,001 - 25,000</option>
												<option value="f">25,001 - 30,000</option>
												<option value="g">Above 30,000</option>
											</select>
											<i></i>
										</label>
										</li>
										<li>
 											<input style="width: 100% !important;" type="submit" name="submit" class="btn-u rounded-2x btn-u-lg">
 										</form>
 									</ul>
 								</div>
 							</div>
 						</div>
 					</div><!--/end panel group-->
 					<div class="panel-group" id="accordion-v4">
 						<div class="panel panel-default">
 							<div style="background-color: #7587b1 !important;border-radius: 8px;font-size: 28px;border-bottom: 1px solid;border-bottom-color: tomato;padding: 3px;box-shadow: 7px 4px 11px -1px #dedede;" class="panel-heading">
 								<h2 class="panel-title">
 									<a data-toggle="collapse" data-parent="#accordion-v5" href="#collapseFive">
 										Latest Additions
 										<i class="fa fa-angle-down"></i>
 									</a>
 								</h2>
 							</div>
 							<div id="collapseFive" class="panel-collapse collapse in">
 								<div style="text-align: left !important;" class="panel-body main_sidebar_gradient">
 									<ul class="list-unstyled checkbox-list">
 									<?php include("connect.php");
			 						$result = mysqli_query($con, "SELECT * FROM mobiles ORDER BY id desc LIMIT 5");
			 						while ($row = mysqli_fetch_array($result)) {
			 							$url = $row['url'];
			 							$url = str_replace(" ", "-", $url);
			 							$mobile_name = $row['dev_name'];
			 							$price = $row['price'];
			 							$image = $row['picture'];
			 							?>
 										<li><a href="<?php echo $url;?>"><img src="<?php echo "$image"; ?>" width="50px" height="50px" alt="<?php echo $mobile_name; ?>"><span style="margin-left: 20px; color: #000; font-size: 12px; line-height: 25px; font-family: 'Open Sans', sans-serif;"><?php echo $mobile_name; ?></span></a>
 										</li>
 									<?php
 									}
 									?>
 									</ul>
 								</div>
 							</div>
 						</div>
 					</div>
 					<div class="panel-group" id="accordion-v5">
 						<div class="panel panel-default">
 						<div style="background-color: #7587b1 !important;border-radius: 8px;font-size: 28px;border-bottom: 1px solid;border-bottom-color: tomato;padding: 3px;box-shadow: 7px 4px 11px -1px #dedede;" class="panel-heading">
 								<h2 class="panel-title">
 									<a href="compare.php" data-toggle="collapse" data-parent="#accordion-v4" href="#collapseFour">
 										Brand Comparison
 										<i class="fa fa-angle-down"></i>
 									</a>
 								</h2>
 							</div>
 							<div id="collapseFour" class="panel-collapse collapse in">
 								<div style="text-align: center;" class="panel-body main_sidebar_gradient">
 									<ul class="list-unstyled checkbox-list">
 										<li>
 											<a style="width: 100% !important;" href="compare.php" class="btn-u rounded-2x btn-u-lg">Compare</a>
 										</li>
 									</ul>
 								</div>
 							</div>
 						</div>
 					</div>
 					<div class="panel-group" id="accordion-v6">
 						<div class="panel panel-default">
 						<div style="background-color: #7587b1 !important;border-radius: 8px;font-size: 28px;border-bottom: 1px solid;border-bottom-color: tomato;padding: 3px;box-shadow: 7px 4px 11px -1px #dedede;" class="panel-heading">
 								<h2 class="panel-title">
 									<a data-toggle="collapse" data-parent="#accordion-v6" href="#collapseSix">
 										Top Rated PHones
 										<i class="fa fa-angle-down"></i>
 									</a>
 								</h2>
 							</div>
 							<div id="collapseSix" class="panel-collapse collapse in">
 								<div style="text-align: left !important;" class="panel-body main_sidebar_gradient">
 									<ul class="list-unstyled checkbox-list">
 										<?php include("connect.php");
 									
			 						$result = mysqli_query($con, "SELECT * FROM mobiles WHERE price > '50000' LIMIT 6");
			 						while ($row = mysqli_fetch_array($result)) {
			 							$url = $row['url'];
			 							$url = str_replace(" ", "-", $url);
			 							$mobile_name = $row['dev_name'];
			 							$price = $row['price'];
			 							$image = $row['picture'];
			 							?>
 										<li><a href="<?php echo $url;?>"><img src="<?php echo "$image"; ?>" width="50px" height="50px" alt="<?php echo $mobile_name; ?>"><span style="margin-left: 20px; color: #000; font-size: 12px; line-height: 25px; font-family: 'Open Sans', sans-serif;"><?php echo $mobile_name; ?></span></a>
 										</li>
 									<?php
 									}
 									?>
 									</ul>
 								</div>
 							</div>
 						</div>
 					</div>

 					<div class="panel-group margin-bottom-30" id="accordion-v7">
 						<div class="panel panel-default">
 						<div style="background-color: #7587b1 !important;border-radius: 8px;font-size: 28px;border-bottom: 1px solid;border-bottom-color: tomato;padding: 3px;box-shadow: 7px 4px 11px -1px #dedede;" class="panel-heading">
 								<h2 class="panel-title">
 									<a data-toggle="collapse" data-parent="#accordion-v7" href="#collapseSeven">
 										Latest Smartphones
 										<i class="fa fa-angle-down"></i>
 									</a>
 								</h2>
 							</div>
 							<div id="collapseSeven" class="panel-collapse collapse in">
 								<div style="text-align: left !important;" class="panel-body main_sidebar_gradient">
 									<ul class="list-unstyled checkbox-list">
 										<?php include("connect.php");
 									
			 						$result = mysqli_query($con, "SELECT * FROM mobiles ORDER BY id ASC LIMIT 5");
			 						while ($row = mysqli_fetch_array($result)) {
			 							$url = $row['url'];
			 							$url = str_replace(" ", "-", $url);
			 							$mobile_name = $row['dev_name'];
			 							$price = $row['price'];
			 							$image = $row['picture'];
			 							?>
 										<li><a href="<?php echo $url;?>"><img src="<?php echo "$image"; ?>" width="50px" height="50px" alt="<?php echo $mobile_name; ?>"><span style="margin-left: 20px; color: #000; font-size: 12px; line-height: 25px; font-family: 'Open Sans', sans-serif;"><?php echo $mobile_name; ?></span></a>
 										</li>
 									<?php
 									}
 									?>
 									</ul>
 								</div>
 							</div>
 						</div>
 					</div>
 				</div>
 				<?php /*include("connect.php");
 				$mobile_names = $urls = $prices = $images = [];
 				$i = 0;
 				$mobiles = mysqli_query($con, "SELECT * FROM main_mobiles ORDER BY id DESC LIMIT 6");
 				while ($row = mysqli_fetch_array($mobiles)) {
 					$brand[$i] = $row['dev_brand'];
 					$mobile_names[$i] = $row['dev_name'];
 					$urls[$i] = $row['url'];
					$prices[$i] = $row['price'];
 					$images[$i] = $row['picture'];
 					$images[$i] = "'$images[$i]'";
 					$i++;
 				}*/
 				?>
 				<div class="col-lg-9 col-md-9 col-sm-12">
 					<h1 style="background-color: #7587b1 !important;border-radius: 8px;color: white;text-shadow: 2px 2px 4px black;font-size: 28px;border-bottom: 1px solid;border-bottom-color: tomato;padding: 8px;box-shadow: 7px 4px 11px -1px #dedede;">APPLE</h1>
 				</div>
				<div class="top-margin col-lg-9 col-md-9">
 						<div class="col-lg-12 col-md-12 col-sm-7 col-xs-12 illustration-v2">
 					<?php
 					include("connect.php");
					/*include_once('functions.php');

					$page = (int)(!isset($_GET["page"]) ? 1 : $_GET["page"]);
					if ($page <= 0) $page = 1;

					$per_page = 30; // Set how many records do you want to display per page.

					$startpoint = ($page * $per_page) - $per_page;*/

					$statement = "`mobiles` WHERE dev_brand = 'Apple' ORDER BY `id` DESC"; // Change `records` according to your table name.
					 
					$results = mysqli_query($con,"SELECT * FROM {$statement} LIMIT 10");

					if (mysqli_num_rows($results) != 0) {
					    
						// displaying records.
					    while ($row = mysqli_fetch_array($results)) {
 							$url = $row['url'];
 							$mobile_name = $row['dev_name'];
 							$url = str_replace(" ", "-", $url);
 							$price = $row['price'];
 							$image = $row['picture'];
 							$thumb = $row['thumb'];
 							?>
 							<div class="col-xs-6 col-sm-4 col-md-2 col-lg-2 devices_list product-img-brd">  
                                <div class="device_box_v2 product-img">                        
                                    <a title="<?php echo $mobile_name; ?>" target="_<?php echo $url;?>" href="<?php echo $url;?>"><img class="img-responsive img-hover" alt="<?php echo $mobile_name; ?>" title="<?php echo $mobile_name; ?>" src="<?php echo $image; ?>" /></a>
                                    
 									<a class="add-to-cart" target="_<?php echo $url;?>" href="<?php echo $url;?>"><!-- <i class="fa fa-shopping-cart"></i> -->PRICE <?php echo $price; ?></a>
                                </div>
                                	<a class="product-review" target="_<?php echo $url;?>" href="<?php echo $url;?>"><?php echo $mobile_name; ?></a>
                            </div>
 						<?php
 							}
 						}
 						else {
					     	echo "No records are found.";
						}
 						?>
 						
 						</div>
 						<!-- <div style="text-align: center;">
 						<?php /*echo pagination($statement,$per_page,$page,$url='?');*/ ?>
 						</div> -->

 					<!-- <div class="text-center">
 						<ul class="pagination pagination-v2">
 							<li><a href="#"><i class="fa fa-angle-left"></i></a></li>
 							<li><a href="#">1</a></li>
 							<li class="active"><a href="#">2</a></li>
 							<li><a href="#">3</a></li>
 							<li><a href="#"><i class="fa fa-angle-right"></i></a></li>
 						</ul>
 					</div> --><!--/end pagination-->
 					<div style="clear: both;"></div>
 					<a href="apple-mobile-prices.php" style="float: right; font-weight: 700; ">More>></a>
 					<hr class="style13">
 					

 				</div>
 				<!-- <div style="clear: both;"></div> -->
 				
 				<div class="col-lg-9 col-md-9 col-sm-12">
 				
 			<h1 style="background-color: #7587b1 !important;border-radius: 8px;color: white;text-shadow: 2px 2px 4px black;font-size: 28px;border-bottom: 1px solid;border-bottom-color: tomato;padding: 8px;box-shadow: 7px 4px 11px -1px #dedede;">SAMSUNG</h1>
 				</div>
				<div class="top-margin col-lg-9 col-md-9">
 						<div class="col-lg-12 col-md-12 col-sm-7 col-xs-12 illustration-v2">
 					<?php
 					include("connect.php");
					/*include_once('functions.php');

					$page = (int)(!isset($_GET["page"]) ? 1 : $_GET["page"]);
					if ($page <= 0) $page = 1;

					$per_page = 30; // Set how many records do you want to display per page.

					$startpoint = ($page * $per_page) - $per_page;*/

					$statement = "`mobiles` WHERE dev_brand = 'Samsung' ORDER BY `id` DESC"; // Change `records` according to your table name.
					 
					$results = mysqli_query($con,"SELECT * FROM {$statement} LIMIT 10");

					if (mysqli_num_rows($results) != 0) {
					    
						// displaying records.
					    while ($row = mysqli_fetch_array($results)) {
 							$url = $row['url'];
 							$mobile_name = $row['dev_name'];
 							$url = str_replace(" ", "-", $url);
 							$price = $row['price'];
 							$image = $row['picture'];
 							$thumb = $row['thumb'];
 							?>
 							<div class="col-xs-6 col-sm-4 col-md-2 col-lg-2 devices_list product-img-brd">  
                                <div class="device_box_v2 product-img">                        
                                    <a title="<?php echo $mobile_name; ?>" target="_<?php echo $url;?>" href="<?php echo $url;?>"><img class="img-responsive img-hover" alt="<?php echo $mobile_name; ?>" title="<?php echo $mobile_name; ?>" src="<?php echo $image; ?>" /></a>
                                    
 									<a class="add-to-cart" target="_<?php echo $url;?>" href="<?php echo $url;?>"><!-- <i class="fa fa-shopping-cart"></i> -->PRICE <?php echo $price; ?></a>
                                </div>
                                	<a class="product-review" target="_<?php echo $url;?>" href="<?php echo $url;?>"><?php echo $mobile_name; ?></a>
                            </div>
 						<?php
 							}
 						}
 						else {
					     	echo "No records are found.";
						}
 						?>
 						
 						</div>
 						<!-- <div style="text-align: center;">
 						<?php /*echo pagination($statement,$per_page,$page,$url='?');*/ ?>
 						</div> -->

 					<!-- <div class="text-center">
 						<ul class="pagination pagination-v2">
 							<li><a href="#"><i class="fa fa-angle-left"></i></a></li>
 							<li><a href="#">1</a></li>
 							<li class="active"><a href="#">2</a></li>
 							<li><a href="#">3</a></li>
 							<li><a href="#"><i class="fa fa-angle-right"></i></a></li>
 						</ul>
 					</div> --><!--/end pagination-->
 				<div style="clear: both;"></div>
 					<a href="samsung-mobile-prices.php" style="float: right; font-weight: 700; ">More>></a>
 					<hr class="style13">
 				</div>
 				<div class="col-lg-9 col-md-9 col-sm-12">
 					<h1 style="background-color: #7587b1 !important;border-radius: 8px;color: white;text-shadow: 2px 2px 4px black;font-size: 28px;border-bottom: 1px solid;border-bottom-color: tomato;padding: 8px;box-shadow: 7px 4px 11px -1px #dedede;">NOKIA</h1>
 				</div>
				<div class="top-margin col-lg-9 col-md-9">
 						<div class="col-lg-12 col-md-12 col-sm-7 col-xs-12 illustration-v2">
 					<?php
 					include("connect.php");
					/*include_once('functions.php');

					$page = (int)(!isset($_GET["page"]) ? 1 : $_GET["page"]);
					if ($page <= 0) $page = 1;

					$per_page = 30; // Set how many records do you want to display per page.

					$startpoint = ($page * $per_page) - $per_page;*/

					$statement = "`mobiles` WHERE dev_brand = 'NOKIA' ORDER BY `id` DESC"; // Change `records` according to your table name.
					 
					$results = mysqli_query($con,"SELECT * FROM {$statement} LIMIT 10");

					if (mysqli_num_rows($results) != 0) {
					    
						// displaying records.
					    while ($row = mysqli_fetch_array($results)) {
 							$url = $row['url'];
 							$mobile_name = $row['dev_name'];
 							$url = str_replace(" ", "-", $url);
 							$price = $row['price'];
 							$image = $row['picture'];
 							$thumb = $row['thumb'];
 							?>
 							<div class="col-xs-6 col-sm-4 col-md-2 col-lg-2 devices_list product-img-brd">  
                                <div class="device_box_v2 product-img">                        
                                    <a title="<?php echo $mobile_name; ?>" target="_<?php echo $url;?>" href="<?php echo $url;?>"><img class="img-responsive img-hover" alt="<?php echo $mobile_name; ?>" title="<?php echo $mobile_name; ?>" src="<?php echo $image; ?>" /></a>
                                    
 									<a class="add-to-cart" target="_<?php echo $url;?>" href="<?php echo $url;?>"><!-- <i class="fa fa-shopping-cart"></i> -->PRICE <?php echo $price; ?></a>
                                </div>
                                	<a class="product-review" target="_<?php echo $url;?>" href="<?php echo $url;?>"><?php echo $mobile_name; ?></a>
                            </div>
 						<?php
 							}
 						}
 						else {
					     	echo "No records are found.";
						}
 						?>
 						
 						</div>
 						<!-- <div style="text-align: center;">
 						<?php /*echo pagination($statement,$per_page,$page,$url='?');*/ ?>
 						</div> -->

 					<!-- <div class="text-center">
 						<ul class="pagination pagination-v2">
 							<li><a href="#"><i class="fa fa-angle-left"></i></a></li>
 							<li><a href="#">1</a></li>
 							<li class="active"><a href="#">2</a></li>
 							<li><a href="#">3</a></li>
 							<li><a href="#"><i class="fa fa-angle-right"></i></a></li>
 						</ul>
 					</div> --><!--/end pagination-->
 				<div style="clear: both;"></div>
 					<a href="htc-mobile-prices.php" style="float: right; font-weight: 700; ">More>></a>
 					<hr class="style13">
 				</div>
 				<div class="col-lg-9 col-md-9 col-sm-12">
 					<h1 style="background-color: #7587b1 !important;border-radius: 8px;color: white;text-shadow: 2px 2px 4px black;font-size: 28px;border-bottom: 1px solid;border-bottom-color: tomato;padding: 8px;box-shadow: 7px 4px 11px -1px #dedede;">HTC</h1>
 				</div>
				<div class="top-margin col-lg-9 col-md-9">
 						<div class="col-lg-12 col-md-12 col-sm-7 col-xs-12 illustration-v2">
 					<?php
 					include("connect.php");
					/*include_once('functions.php');

					$page = (int)(!isset($_GET["page"]) ? 1 : $_GET["page"]);
					if ($page <= 0) $page = 1;

					$per_page = 30; // Set how many records do you want to display per page.

					$startpoint = ($page * $per_page) - $per_page;*/

					$statement = "`mobiles` WHERE dev_brand = 'HTC' ORDER BY `id` DESC"; // Change `records` according to your table name.
					 
					$results = mysqli_query($con,"SELECT * FROM {$statement} LIMIT 10");

					if (mysqli_num_rows($results) != 0) {
					    
						// displaying records.
					    while ($row = mysqli_fetch_array($results)) {
 							$url = $row['url'];
 							$mobile_name = $row['dev_name'];
 							$url = str_replace(" ", "-", $url);
 							$price = $row['price'];
 							$image = $row['picture'];
 							$thumb = $row['thumb'];
 							?>
 							<div class="col-xs-6 col-sm-4 col-md-2 col-lg-2 devices_list product-img-brd">  
                                <div class="device_box_v2 product-img">                        
                                    <a title="<?php echo $mobile_name; ?>" target="_<?php echo $url;?>" href="<?php echo $url;?>"><img class="img-responsive img-hover" alt="<?php echo $mobile_name; ?>" title="<?php echo $mobile_name; ?>" src="<?php echo $image; ?>" /></a>
                                    
 									<a class="add-to-cart" target="_<?php echo $url;?>" href="<?php echo $url;?>"><!-- <i class="fa fa-shopping-cart"></i> -->PRICE <?php echo $price; ?></a>
                                </div>
                                	<a class="product-review" target="_<?php echo $url;?>" href="<?php echo $url;?>"><?php echo $mobile_name; ?></a>
                            </div>
 						<?php
 							}
 						}
 						else {
					     	echo "No records are found.";
						}
 						?>
 						
 						</div>
 						<!-- <div style="text-align: center;">
 						<?php /*echo pagination($statement,$per_page,$page,$url='?');*/ ?>
 						</div> -->

 					<!-- <div class="text-center">
 						<ul class="pagination pagination-v2">
 							<li><a href="#"><i class="fa fa-angle-left"></i></a></li>
 							<li><a href="#">1</a></li>
 							<li class="active"><a href="#">2</a></li>
 							<li><a href="#">3</a></li>
 							<li><a href="#"><i class="fa fa-angle-right"></i></a></li>
 						</ul>
 					</div> --><!--/end pagination-->
 				<div style="clear: both;"></div>
 					<a href="htc-mobile-prices.php" style="float: right; font-weight: 700; ">More>></a>
 					<hr class="style13">
 				</div>
 				<div class="col-lg-3 col-md-3 col-sm-3">
 				</div>
 				<div class="col-lg-9 col-md-9 col-sm-12">
 					<h1 style="background-color: #7587b1 !important;border-radius: 8px;color: white;text-shadow: 2px 2px 4px black;font-size: 28px;border-bottom: 1px solid;border-bottom-color: tomato;padding: 8px;box-shadow: 7px 4px 11px -1px #dedede;">SONY</h1>
 				</div>
 				<div class="col-lg-3 col-md-3 col-sm-3">
 				</div>
				<div class="top-margin col-lg-9 col-md-9">
 						<div class="col-lg-12 col-md-12 col-sm-7 col-xs-12 illustration-v2">
 					<?php
 					include("connect.php");
					/*include_once('functions.php');

					$page = (int)(!isset($_GET["page"]) ? 1 : $_GET["page"]);
					if ($page <= 0) $page = 1;

					$per_page = 30; // Set how many records do you want to display per page.

					$startpoint = ($page * $per_page) - $per_page;*/

					$statement = "`mobiles` WHERE dev_brand = 'Sony' ORDER BY `id` DESC"; // Change `records` according to your table name.
					 
					$results = mysqli_query($con,"SELECT * FROM {$statement} LIMIT 10");

					if (mysqli_num_rows($results) != 0) {
					    
						// displaying records.
					    while ($row = mysqli_fetch_array($results)) {
 							$url = $row['url'];
 							$mobile_name = $row['dev_name'];
 							$url = str_replace(" ", "-", $url);
 							$price = $row['price'];
 							$image = $row['picture'];
 							$thumb = $row['thumb'];
 							?>
 							<div class="col-xs-6 col-sm-4 col-md-2 col-lg-2 devices_list product-img-brd">  
                                <div class="device_box_v2 product-img">                        
                                    <a title="<?php echo $mobile_name; ?>" target="_<?php echo $url;?>" href="<?php echo $url;?>"><img class="img-responsive img-hover" alt="<?php echo $mobile_name; ?>" title="<?php echo $mobile_name; ?>" src="<?php echo $image; ?>" /></a>
                                    
 									<a class="add-to-cart" target="_<?php echo $url;?>" href="<?php echo $url;?>"><!-- <i class="fa fa-shopping-cart"></i> -->PRICE <?php echo $price; ?></a>
                                </div>
                                	<a class="product-review" target="_<?php echo $url;?>" href="<?php echo $url;?>"><?php echo $mobile_name; ?></a>
                            </div>
 						<?php
 							}
 						}
 						else {
					     	echo "No records are found.";
						}
 						?>
 						
 						</div>
 						<!-- <div style="text-align: center;">
 						<?php /*echo pagination($statement,$per_page,$page,$url='?');*/ ?>
 						</div> -->

 					<!-- <div class="text-center">
 						<ul class="pagination pagination-v2">
 							<li><a href="#"><i class="fa fa-angle-left"></i></a></li>
 							<li><a href="#">1</a></li>
 							<li class="active"><a href="#">2</a></li>
 							<li><a href="#">3</a></li>
 							<li><a href="#"><i class="fa fa-angle-right"></i></a></li>
 						</ul>
 					</div> --><!--/end pagination-->
 				<div style="clear: both;"></div>
 					<a href="sony-mobile-prices.php" style="float: right; font-weight: 700; ">More>></a>
 					<hr class="style13">
 				</div>
 			</div><!--/end row-->	
 		</div><!--/end container-->
 		<!--=== End Content Part ===-->

		<ul class="list-inline owl-slider-v2">
			<li class="item first-child">
				<a href="apple-mobile-prices.php" target="apple-mobile-prices.php"><img src="smartphones/iphone2.jpg" alt=""></a>
			</li>
			<li class="item">
				<a href="samsung-mobile-prices.php" target="samsung-mobile-prices.php"><img src="smartphones/samsung.jpg" alt=""></a>
			</li>
			<li class="item">
				<a href="lg-mobile-prices.php" target="lg-mobile-prices.php"><img src="smartphones/lg-logo.jpg" alt=""></a>
			</li>
			<li class="item">
				<a href="htc-mobile-prices.php" target="htc-mobile-prices.php"><img src="smartphones/htc.jpg" alt=""></a>
			</li>
			<li class="item">
				<a href="sony-mobile-prices.php" target="sony-mobile-prices.php"><img src="smartphones/sony.jpg" alt=""></a>
			</li>
			<li class="item">
				<a href="ericsson-mobile-prices.php" target="ericsson-mobile-prices.php"><img src="smartphones/ericsson.jpg" alt=""></a>
			</li>
			<li class="item">
				<a href="qmobile-mobile-prices.php" target="qmobile-mobile-prices.php"><img src="smartphones/qmobile.jpg" alt=""></a>
			</li>
			<li class="item">
				<a href="nokia-mobile-prices.php" target="nokia-mobile-prices.php"><img src="smartphones/nokia.jpg" alt=""></a>
			</li>
			<!-- <li class="item">
				<img src="assets/img/clients/09.png" alt="">
			</li>
			<li class="item">
				<img src="assets/img/clients/10.png" alt="">
			</li>
			<li class="item">
				<img src="assets/img/clients/11.png" alt="">
			</li>
			<li class="item">
				<img src="assets/img/clients/12.png" alt="">
			</li> -->
		</ul><!--/end owl-carousel-->
	</div>
	<!--=== End Sponsors ===-->

	<!--=== Shop Suvbscribe ===-->
	<!-- <div class="shop-subscribe">
		<div class="container">
			<div class="row">
				<div class="col-md-8 md-margin-bottom-20">
					<h2>subscribe to our weekly <strong>newsletter</strong></h2>
				</div>
				<div class="col-md-4">
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Email your email...">
						<span class="input-group-btn">
							<button class="btn" type="button"><i class="fa fa-envelope-o"></i></button>
						</span>
					</div>
				</div>
			</div>
		</div>
	</div> -->
	<!--=== End Shop Suvbscribe ===-->

	<?php include("footer_main.php"); ?>
</div><!--/wrapper-->

<!-- Wait Block -->
<!-- <div class="g-popup-wrapper">
	<div class="g-popup g-popup--discount2">
		<div class="g-popup--discount2-message">
			<h3>Want 10% Off?</h3>
			<h4>You Are Fabulous!</h4>
			<p>Get 10% Off Your Next Purchase! Just Type Email Below!</p>

			<form action="#" class="sky-form">
				<label class="input">
					<input type="email" placeholder="Email" class="form-control">
				</label>
				<label class="input">
					<button class="btn btn-default" type="button">Subscribe</button>
				</label>
			</form>
		</div>
		<img src="assets/img/blog/26.jpg" alt="ALT" width="270">
		<a href="javascript:void(0);" class="g-popup__close g-popup--discount2__close"><span class="icon-close" aria-hidden="true"></span></a>
	</div>
</div> -->
<!-- End Wait Block -->

<!-- JS Global Compulsory -->
<script src="assets/plugins/jquery/jquery.min.js"></script>
<script src="assets/plugins/jquery/jquery-migrate.min.js"></script>
<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/plugins/noUiSlider/jquery.nouislider.all.min.js"></script>
<!-- JS Implementing Plugins -->
<script src="assets/plugins/back-to-top.js"></script>
<script src="assets/plugins/smoothScroll.js"></script>
<script src="assets/plugins/jquery.parallax.js"></script>
<script src="assets/plugins/owl-carousel/owl-carousel/owl.carousel.js"></script>
<script src="assets/plugins/scrollbar/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="assets/plugins/revolution-slider/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script src="assets/plugins/revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script src="assets/plugins/sky-forms-pro/skyforms/js/jquery-ui.min.js"></script>
<!-- JS Customization -->
<script src="assets/js/custom.js"></script>
<!-- JS Page Level -->
<script src="assets/js/shop.app.js"></script>
<script src="assets/js/plugins/owl-carousel.js"></script>
<script src="assets/js/plugins/revolution-slider.js"></script>
<script src="assets/js/plugins/style-switcher.js"></script>
<script type="text/javascript" src="assets/js/plugins/ladda-buttons.js"></script>
<script>
	jQuery(document).ready(function() {
		App.init();
		App.initScrollBar();
		App.initParallaxBg();
		OwlCarousel.initOwlCarousel();
		RevolutionSlider.initRSfullWidth();
		//StyleSwitcher.initStyleSwitcher();
	});
</script>
<!--[if lt IE 9]>
	<script src="assets/plugins/respond.js"></script>
	<script src="assets/plugins/html5shiv.js"></script>
	<script src="assets/js/plugins/placeholder-IE-fixes.js"></script>
	<![endif]-->

</body>

</html>
