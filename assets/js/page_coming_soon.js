var PageComingSoon = function () {
    return {
      //Coming Soon
      initPageComingSoon: function () {
			var today = new Date();
            today.setHours(today.getHours() + 4);
			
			$('#defaultCountdown').countdown({until: today})
        }
    };
}();
