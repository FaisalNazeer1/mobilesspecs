<!DOCTYPE html>

<html lang="en">



<head>

  <title>All Smartphones</title>



  <!-- Meta -->

  <meta charset="utf-8">

  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <meta name="description" content="All Smartphones">

  <meta name="author" content="">
  <meta name="keywords" content="Smartphones">


  <!-- Favicon -->

  <link rel="shortcut icon" href="favicon.ico">



  <!-- Web Fonts -->

  <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,800&amp;subset=cyrillic,latin'>



  <!-- CSS Global Compulsory -->

  <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">

  <link rel="stylesheet" href="assets/css/shop.style.css">



  <!-- CSS Header and Footer -->

  <link rel="stylesheet" href="assets/css/headers/header-v5.css">

  <link rel="stylesheet" href="assets/css/footers/footer-v4.css">



  <!-- CSS Implementing Plugins -->

  <link rel="stylesheet" href="assets/plugins/animate.css">

  <link rel="stylesheet" href="assets/plugins/line-icons/line-icons.css">

  <link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.min.css">

  <link rel="stylesheet" href="assets/plugins/scrollbar/css/jquery.mCustomScrollbar.css">

  <link rel="stylesheet" href="assets/plugins/owl-carousel/owl-carousel/owl.carousel.css">

  <link rel="stylesheet" href="assets/plugins/revolution-slider/rs-plugin/css/settings.css">



  <!-- CSS Theme -->

  <link rel="stylesheet" href="assets/css/theme-colors/default.css" id="style_color">

  <link rel="stylesheet" href="assets/plugins/ladda-buttons/css/custom-lada-btn.css">

  <link rel="stylesheet" href="assets/plugins/hover-effects/css/custom-hover-effects.css">

  <link rel="stylesheet" href="search_css/style.css">

  <script type="text/javascript" src="http://www.google.com/jsapi"></script>

  <script type="text/javascript" src="search_js/script.js"></script>

  

  <!-- CSS Customization -->

  <link rel="stylesheet" href="assets/css/custom.css">

<style type="text/css">
html { -moz-transform: scale(0.8, 0.8); zoom: 0.8; zoom: 80%; }
.container {
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
   -moz-transform: scale(1, 1); zoom: 1; zoom: 100% !important; 
}
.hover {
  background-color: white;
  color: black;
}
.hoverfb {
  color: #3b5998 !important;
}
.hoverfb:hover {
  background-color: #3b5998 !important;
  color: white !important;
}
.hoverfb:hover .fa{
  color: white !important;
}
.hovergp {
  color: #d34836;
}
.hovergp:hover {
  background-color: #d34836 !important;
  color: white !important;
}
.hovergp:hover .fa{
  color: white !important;
}

.hovertw {
  color: skyblue;
}
.hovertw:hover{
  background-color: skyblue !important;
}
.hovertw:hover .fa{
  color: white !important;
}
.hoveryt {
  color: white;
}
.hoveryt:hover {
  background-color: red !important;
  color: white !important;
}
.hoveryt:hover .fa{
  color: white !important;
}
@keyframes bounce {
  0%, 20%, 60%, 100% {
    -webkit-transform: translateY(0);
    transform: translateY(0);
  }

  40% {
    -webkit-transform: translateY(-12px);
    transform: translateY(-12px);
  }

  80% {
    -webkit-transform: translateY(-6px);
    transform: translateY(-6px);
  }
}
.dropdown-toggle {
  font-family: 'Google-Oswald', Arial, sans-serif !important;
}
.dropdown-toggle:hover {
  animation: bounce 0.9s;
  color: white !important;
  font-weight: 600 !important;
  font-size: 24px !important;
  scale: ;
  -webkit-transition: all 0.7s ease;
  transition: all 0.7s ease;
}
.dropdown {
  margin-top: -38px;
}
.nav {
  color: white !important; margin-top: -40px;
}
.sft {
  font-size: 28px !important; margin-top: 10px !important;
}
.rounded-2x {
  font-weight: 600; 
  background-color: #aac9da;
}
.rounded-2x:hover {
  color: black !important;
  background-color: #6694ae !important;
  font-weight: 700;
}
@media screen and (min-width: 250px) and (max-width: 760px){
  #logo {
    display: none;
  }
  #social-icons {
    display: none;
  }
}
@media screen and (min-width: 350px) and (max-width: 760px){
  #logo {
    display: none;
  }
  #social-icons {
    display: none;
  }
  .dropdown {
    margin-top: 0px;
  }
  .nav {
    color: white !important; margin-top: 0px;
  }
  .sft {
    font-size: 22px !important; margin-top: 90px !important;
  }
  .filter-by-block {
    width: 100% !important;
  }
  .brands {
    margin-left: 10% !important;
  }
  .product-img {
    margin: 0 auto !important;
  }
  .navbar-bra {
    display: none;
  }
  .logo-header {
    display: none;
  }
  .devices_list {
    margin-left: 0px !important;
  }
  .device_box_v2 {
    margin-left: 30px;
  }
  .contentcontainer {
    height: 150px !important;
  }
}
.icon-bar {
  width: 200px !important;
}
.center {
    text-align: center;
    padding-bottom: 15px;
}
.fadeInDown {
    -webkit-animation-name: fadeInDown;
    animation-name: fadeInDown;
}
.animated {
    -webkit-animation-duration: 1s;
    animation-duration: 1s;
    -webkit-animation-fill-mode: both;
    animation-fill-mode: both;
}
p {
    margin: 0 0 10px;
}
.two_cols {
    overflow: hidden;
}
.list-unstyled {
    padding-left: 0;
    list-style: none;
}
.clear {
  clear: both;
}
li {
    display: list-item;
    text-align: -webkit-match-parent;
}
ol, ul {
    margin-top: 0;
    margin-bottom: 10px;
}
.two_cols > li {
  float: left;
  width: 50%;
  padding-top: 1px;
}
.brands_list .two_cols > li {
  padding-bottom: 20px;
}

.brands_list img {
  margin-right:10px;
  width:92px;
  height:22px;
}
.brands_list a {
    font-size: 24px;
    color: #428bca !important;
    text-decoration: none;
}
img {
    vertical-align: middle;
}
img {
    border: 0;
}
</style>

</head>



<body class="header-fixed">

  <div class="wrapper">

  <?php include("header_device.php"); ?>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" align="center" style="margin-top:20px; margin-bottom:20px;">
      <div style="width: 100% !important; background-color: lightskyblue;height: 100px;vertical-align: middle;
    text-align: center;" class="center wow fadeInDown">
        <h1 style="color: white; font-family: oswald; font-size: 50px; vertical-align: middle;height: 100px; padding-top: 30px; text-shadow: 4px 4px 4px black; font-weight: 600;">All Smartphones Brands</h1>
      </div>
      <div class="container">

      <div class="wow fadeInDown">
        <p><br></p>
                
                <div style="text-align: left;" class="brands_list">
                <ul class="list-unstyled two_cols">
                  <li><img src="content/media/img/a6c6239c-acer.gif" /> <a title="Acer phones" href="acer/index.html">Acer phones (96)</a></li>
                  <li><img src="content/media/img/486d7541-alcatel.gif" /> <a title="Alcatel phones" href="alcatel/index.html">Alcatel phones (343)</a></li>
                  <li><img src="content/media/img/4d2723c8-allview.gif" /> <a title="Allview phones" href="allview/index.html">Allview phones (103)</a></li>
                  <li><img src="content/media/img/f9c1ec7c-amazon.gif" /> <a title="Amazon phones" href="amazon/index.html">Amazon phones (14)</a></li>
                  <li><img src="content/media/img/3071ad75-amoi.gif" /> <a title="Amoi phones" href="amoi/index.html">Amoi phones (47)</a></li>
                  <li><img src="content/media/img/46b347a5-apple.gif" /> <a title="Apple phones" href="apple/index.html">Apple phones (38)</a></li>
                  <li><img src="content/media/img/906a58e9-archos.gif" /> <a title="Archos phones" href="archos/index.html">Archos phones (25)</a></li>
                  <li><img src="content/media/img/ef8f838b-asus.gif" /> <a title="Asus phones" href="asus/index.html">Asus phones (127)</a></li>
                  <li><img src="content/media/img/b2b1ec69-att.gif" /> <a title="AT&amp;T phones" href="att/index.html">AT&amp;T phones (4)</a></li>
                  <li><img src="content/media/img/494db264-benefon.gif" /> <a title="Benefon phones" href="benefon/index.html">Benefon phones (8)</a></li>
                  <li><img src="content/media/img/ee8d9133-benq.gif" /> <a title="BenQ phones" href="benq/index.html">BenQ phones (25)</a></li>
                  <li><img src="content/media/img/094226b5-benqsiemens.gif" /> <a title="BenQ-Siemens phones" href="benq-siemens/index.html">BenQ-Siemens phones (27)</a></li>
                  <li><img src="content/media/img/cba76e7f-bird.gif" /> <a title="Bird phones" href="bird/index.html">Bird phones (60)</a></li>
                  <li><img src="content/media/img/43d91554-bberry.gif" /> <a title="Blackberry phones" href="blackberry/index.html">Blackberry phones (83)</a></li>
                  <li><img src="content/media/img/7f385ba9-blu.gif" /> <a title="BLU phones" href="blu/index.html">BLU phones (187)</a></li>
                  <li><img src="content/media/img/bb13a682-bosch.gif" /> <a title="Bosch phones" href="bosch/index.html">Bosch phones (10)</a></li>
                  <li><img src="content/media/img/ad6e82b0-bq.gif" /> <a title="BQ phones" href="bq/index.html">BQ phones (0)</a></li>
                  <li><img src="content/media/img/50da2165-celkon.gif" /> <a title="Celkon phones" href="celkon/index.html">Celkon phones (223)</a></li>
                  <li><img src="content/media/img/b700063f-dell.gif" /> <a title="Dell phones" href="dell/index.html">Dell phones (20)</a></li>
                  <li><img src="content/media/img/7110a62d-emporia.gif" /> <a title="Emporia phones" href="emporia/index.html">Emporia phones (15)</a></li>
                  <li><img src="content/media/img/9a07beac-ericsson.gif" /> <a title="Ericsson phones" href="ericsson/index.html">Ericsson phones (40)</a></li>
                  <li><img src="content/media/img/cbb7b42e-gigabyte.gif" /> <a title="Gigabyte phones" href="gigabyte/index.html">Gigabyte phones (63)</a></li>
                  <li><img src="content/media/img/8690951f-gionee.gif" /> <a title="Gionee phones" href="gionee/index.html">Gionee phones (52)</a></li>
                  <li><img src="content/media/img/557b3148-haier.gif" /> <a title="Haier phones" href="haier/index.html">Haier phones (50)</a></li>
                  <li><img src="content/media/img/04a2ae53-hp.gif" /> <a title="HP phones" href="hp/index.html">HP phones (41)</a></li>
                  <li><img src="content/media/img/066ec483-htc.gif" /> <a title="HTC phones" href="htc/index.html">HTC phones (233)</a></li>
                  <li><img src="content/media/img/f1cbbc04-huawei.gif" /> <a title="Huawei phones" href="huawei/index.html">Huawei phones (199)</a></li>
                  <li><img src="content/media/img/c88f53b3-imate.gif" /> <a title="i-mate phones" href="i-mate/index.html">i-mate phones (34)</a></li>
                  <li><img src="content/media/img/f8f092b8-icemobile.gif" /> <a title="Icemobile phones" href="icemobile/index.html">Icemobile phones (60)</a></li>
                  <li><img src="content/media/img/af36296d-intex.gif" /> <a title="Intex phones" href="intex/index.html">Intex phones (14)</a></li>
                  <li><img src="content/media/img/96cab98d-karbonn.gif" /> <a title="Karbonn phones" href="karbonn/index.html">Karbonn phones (57)</a></li>
                  <li><img src="content/media/img/a24319a8-kyocera.gif" /> <a title="Kyocera phones" href="kyocera/index.html">Kyocera phones (22)</a></li>
                  <li><img src="content/media/img/a44a0aba-lava.gif" /> <a title="Lava phones" href="lava/index.html">Lava phones (79)</a></li>
                  <li><img src="content/media/img/d9e58266-lenovo.gif" /> <a title="Lenovo phones" href="lenovo/index.html">Lenovo phones (144)</a></li>
                  <li><img src="content/media/img/c5d33fa7-lg-lg.gif" /> <a title="LG phones" href="lg/index.html">LG phones (572)</a></li>
                  <li><img src="content/media/img/ad503e7a-maxwest.gif" /> <a title="Maxwest phones" href="maxwest/index.html">Maxwest phones (37)</a></li>
                  <li><img src="content/media/img/0eb4db39-meizu.gif" /> <a title="Meizu phones" href="meizu/index.html">Meizu phones (18)</a></li>
                  <li><img src="content/media/img/f49f9214-micromax.gif" /> <a title="Micromax phones" href="micromax/index.html">Micromax phones (235)</a></li>
                  <li><img src="content/media/img/5ba9dc51-microsoft.gif" /> <a title="Microsoft phones" href="microsoft/index.html">Microsoft phones (27)</a></li>
                  <li><img src="content/media/img/56be0680-motorola.gif" /> <a title="Motorola phones" href="motorola/index.html">Motorola phones (427)</a></li>
                  <li><img src="content/media/img/bc379bf6-niu.gif" /> <a title="NIU phones" href="niu/index.html">NIU phones (30)</a></li>
                  <li><img src="content/media/img/f9af010d-nokia.gif" /> <a title="Nokia phones" href="nokia/index.html">Nokia phones (439)</a></li>
                  <li><img src="content/media/img/af403fb1-o2.gif" /> <a title="O2 phones" href="o2/index.html">O2 phones (45)</a></li>
                  <li><img src="content/media/img/875fa66a-oneplus.gif" /> <a title="Oneplus phones" href="oneplus/index.html">Oneplus phones (4)</a></li>
                  <li><img src="content/media/img/7a9e49f8-oppo.gif" /> <a title="Oppo phones" href="oppo/index.html">Oppo phones (49)</a></li>
                  <li><img src="content/media/img/6971a9be-orange.gif" /> <a title="Orange phones" href="orange/index.html">Orange phones (18)</a></li>
                  <li><img src="content/media/img/79086294-panas.gif" /> <a title="Panasonic phones" href="panasonic/index.html">Panasonic phones (82)</a></li>
                  <li><img src="content/media/img/da06a796-pantech.gif" /> <a title="Pantech phones" href="pantech/index.html">Pantech phones (72)</a></li>
                  <li><img src="content/media/img/f03ca479-philips.gif" /> <a title="Philips phones" href="philips/index.html">Philips phones (196)</a></li>
                  <li><img src="content/media/img/273515ba-plum.gif" /> <a title="Plum phones" href="plum/index.html">Plum phones (85)</a></li>
                  <li><img src="content/media/img/57ddeca2-posh.gif" /> <a title="Posh phones" href="posh/index.html">Posh phones (19)</a></li>
                  <li><img src="content/media/img/1fa16eba-prestigio.gif" /> <a title="Prestigio phones" href="prestigio/index.html">Prestigio phones (40)</a></li>
                  <li><img src="content/media/img/9ad9c38e-qmobile.gif" /> <a title="QMobile phones" href="qmobile/index.html">QMobile phones (71)</a></li>
                  <li><img src="content/media/img/787fbaee-samsung.gif" /> <a title="Samsung phones" href="samsung/index.html">Samsung phones (1018)</a></li>
                  <li><img src="content/media/img/8866b051-sharp.gif" /> <a title="Sharp phones" href="sharp/index.html">Sharp phones (43)</a></li>
                  <li><img src="content/media/img/1ecd7290-sony.gif" /> <a title="Sony phones" href="sony/index.html">Sony phones (111)</a></li>
                  <li><img src="content/media/img/e3745e05-spice.gif" /> <a title="Spice phones" href="spice/index.html">Spice phones (120)</a></li>
                  <li><img src="content/media/img/cc9b68d1-tmobile.gif" /> <a title="T-Mobile phones" href="t-mobile/index.html">T-Mobile phones (52)</a></li>
                  <li><img src="content/media/img/7bf026f7-toshiba.gif" /> <a title="Toshiba phones" href="toshiba/index.html">Toshiba phones (35)</a></li>
                  <li><img src="content/media/img/32c7a9a7-unnecto.gif" /> <a title="Unnecto phones" href="unnecto/index.html">Unnecto phones (24)</a></li>
                  <li><img src="content/media/img/69b5a63e-vertu.gif" /> <a title="Vertu phones" href="vertu/index.html">Vertu phones (17)</a></li>
                  <li><img src="content/media/img/da40c219-verykool.gif" /> <a title="verykool phones" href="verykool/index.html">verykool phones (106)</a></li>
                  <li><img src="content/media/img/c0f77a3c-vivo.gif" /> <a title="vivo phones" href="vivo/index.html">vivo phones (29)</a></li>
                  <li><img src="content/media/img/4a90aca2-vodafone.gif" /> <a title="Vodafone phones" href="vodafone/index.html">Vodafone phones (75)</a></li>
                  <li><img src="content/media/img/b36a2aad-wiko.gif" /> <a title="Wiko phones" href="wiko/index.html">Wiko phones (28)</a></li>
                  <li><img src="content/media/img/50f38748-xiaomi.gif" /> <a title="Xiaomi phones" href="xiaomi/index.html">Xiaomi phones (31)</a></li>
                  <li><img src="content/media/img/c5f20154-xolo.gif" /> <a title="XOLO phones" href="xolo/index.html">XOLO phones (78)</a></li>
                  <li><img src="content/media/img/dbf97f5d-yezz.gif" /> <a title="Yezz phones" href="yezz/index.html">Yezz phones (0)</a></li>
                  <li><img src="content/media/img/c6f54f0b-yu.gif" /> <a title="YU phones" href="yu/index.html">YU phones (0)</a></li>
                  <li><img src="content/media/img/89dc3240-zte.gif" /> <a title="ZTE phones" href="zte/index.html">ZTE phones (191)</a></li>
                </ul>
                </div>
                
      </div>  
    </div><!--/.container-->
    </div>
    <div class="clear"></div>
  </div><!--/end wrapper-->
<?php include("footer.php"); ?>
<!-- JS Global Compulsory -->

<script src="assets/plugins/jquery/jquery.min.js"></script>

<script src="assets/plugins/jquery/jquery-migrate.min.js"></script>

<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>

<script src="assets/plugins/noUiSlider/jquery.nouislider.all.min.js"></script>

<!-- JS Implementing Plugins -->

<script src="assets/plugins/back-to-top.js"></script>

<script src="assets/plugins/smoothScroll.js"></script>

<script src="assets/plugins/jquery.parallax.js"></script>

<script src="assets/plugins/owl-carousel/owl-carousel/owl.carousel.js"></script>

<script src="assets/plugins/scrollbar/js/jquery.mCustomScrollbar.concat.min.js"></script>

<script src="assets/plugins/revolution-slider/rs-plugin/js/jquery.themepunch.tools.min.js"></script>

<script src="assets/plugins/revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

<script src="assets/plugins/sky-forms-pro/skyforms/js/jquery-ui.min.js"></script>

<!-- JS Customization -->

<script src="assets/js/custom.js"></script>

<!-- JS Page Level -->

<script src="assets/js/shop.app.js"></script>

<script src="assets/js/plugins/owl-carousel.js"></script>

<script src="assets/js/plugins/revolution-slider.js"></script>

<script>

  jQuery(document).ready(function() {

    App.init();

    App.initScrollBar();

    App.initParallaxBg();

    OwlCarousel.initOwlCarousel();

    RevolutionSlider.initRSfullWidth();

    //StyleSwitcher.initStyleSwitcher();

  });

</script>

</body>

</html>