-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 04, 2017 at 10:27 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `saba_sellers`
--

-- --------------------------------------------------------

--
-- Table structure for table `branches`
--

CREATE TABLE `branches` (
  `branch_id` int(11) NOT NULL,
  `branch_name` varchar(100) NOT NULL,
  `branch_address` varchar(100) NOT NULL,
  `branch_city` varchar(50) NOT NULL,
  `branch_country` varchar(50) NOT NULL,
  `branch_phone` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `branches`
--

INSERT INTO `branches` (`branch_id`, `branch_name`, `branch_address`, `branch_city`, `branch_country`, `branch_phone`) VALUES
(1, 'Rawalpindi', 'Bank Road, Saddar', 'Rawalpindi', 'Pakistan', '+92515678433'),
(2, 'Lahore Mall', 'Mall Road', 'Lahore', 'Pakistan', '+924273498747'),
(8, 'test', 'sdfsd fsfs fs', 'rwp', 'Pakistan', '434342343');

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--

CREATE TABLE `brand` (
  `id` int(11) NOT NULL,
  `brand_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brand`
--

INSERT INTO `brand` (`id`, `brand_name`) VALUES
(2, 'Nokia'),
(3, 'Samsung'),
(5, 'Apple'),
(6, 'Hitachi');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `cat_name` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `cat_name`) VALUES
(1, 'Home Appliance'),
(2, 'LCDS'),
(3, 'TV'),
(5, 'Mobile'),
(6, 'Laptops');

-- --------------------------------------------------------

--
-- Table structure for table `clear_installments`
--

CREATE TABLE `clear_installments` (
  `id` int(11) NOT NULL,
  `sale_id` int(11) NOT NULL,
  `date` varchar(50) NOT NULL,
  `amount` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clear_installments`
--

INSERT INTO `clear_installments` (`id`, `sale_id`, `date`, `amount`) VALUES
(11, 21, 'Jun-04-2017', '5000'),
(12, 25, 'Jun-13-2017', '5000'),
(17, 30, 'Jun-13-2017', '6583'),
(18, 30, 'Jul-13-2017', '6583'),
(19, 30, 'Aug-13-2017', '6583'),
(36, 30, 'Jun-15-2017', '6583'),
(50, 1, 'Jun-15-2017', '8333');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `customer_id` int(11) NOT NULL,
  `customer_name` varchar(100) NOT NULL,
  `customer_cnic` varchar(100) NOT NULL,
  `customer_address` varchar(100) NOT NULL,
  `customer_city` varchar(50) NOT NULL,
  `customer_country` varchar(50) NOT NULL,
  `customer_phone` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`customer_id`, `customer_name`, `customer_cnic`, `customer_address`, `customer_city`, `customer_country`, `customer_phone`) VALUES
(1, 'Sultan Ahmed', '344443-5-2323', 'H. No. 3, Street 6, Murree Road', 'Rawalpindi', 'Pakistan', '+92387646347'),
(2, 'testtset', '4454534333333', 'sfsdfsdf', 'RWP', 'Pakistan', '45333345554'),
(3, 'test', '3534534534534', 'sdf sdfs dfsd fsdf', 'RWP', 'Pakistan', '34534534534'),
(4, 'sddf', '2342342342342', 'sfs fsdf sdf fsd f', 'RWP', 'Pakistan', '23423423423'),
(5, 'M Hanif', '4534534534534', 'sdfsdfsdf sdfsdfdfs df ', 'RWP', 'Pakistan', '34534534534'),
(6, 'M Jahangir', '5345345345345', 'sdfsdf sfsdf sfsd fsdf', 'RWP', 'Pakistan', '34534534534'),
(7, 'M ABCD', '2222222222222', 'jhkjhjkhkjh jkhjkhjkh jhkljhkljhklj', 'RWP', 'Pakistan', '22222222222'),
(8, 'sdfsdfsfsdf', '', '', 'RWP', 'Pakistan', ''),
(9, 'asdasda', '', '', 'RWP', 'Pakistan', ''),
(10, 'hkjhkjhjkh', '', '', 'RWP', 'Pakistan', ''),
(11, 'MyName', '1234567891012', '', 'RWP', 'Pakistan', ''),
(12, 'MyName222', '1234567891012', '', 'RWP', 'Pakistan', ''),
(13, 'sdfsdfsf', '4444444444444', 'sdaf sdfsadfas dfsf ', 'RWP', 'Pakistan', '44444444444'),
(14, 'sfsfsf', '3333333333333', 'dfsdfasd fsdf sdfs df', 'RWP', 'Pakistan', '33333333333');

-- --------------------------------------------------------

--
-- Table structure for table `installment_plans`
--

CREATE TABLE `installment_plans` (
  `id` int(11) NOT NULL,
  `plan_name` varchar(60) NOT NULL,
  `down_payment` varchar(60) NOT NULL,
  `admin_fee` varchar(20) NOT NULL,
  `first_payment` varchar(20) NOT NULL,
  `rebate` varchar(20) NOT NULL,
  `surcharge` varchar(20) NOT NULL,
  `time_period` varchar(30) NOT NULL,
  `mon_installment` varchar(30) NOT NULL,
  `interest` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE `invoice` (
  `id` int(11) NOT NULL,
  `invoice_no` varchar(60) NOT NULL,
  `vendor` varchar(150) NOT NULL,
  `rec_date` varchar(50) NOT NULL,
  `item_name` varchar(150) NOT NULL,
  `item_model` varchar(150) NOT NULL,
  `brand_name` varchar(100) NOT NULL,
  `item_quantity` varchar(30) NOT NULL,
  `wholesale_price` varchar(30) NOT NULL,
  `discount_price` varchar(30) NOT NULL,
  `net_amount` varchar(30) NOT NULL,
  `total_amount` varchar(30) NOT NULL,
  `transport_charges` int(30) NOT NULL,
  `other_charges` int(30) NOT NULL,
  `total_invoice_amount` int(50) NOT NULL,
  `pre_pay_method` varchar(30) NOT NULL,
  `pre_amount` int(50) NOT NULL,
  `this_invoice` int(50) NOT NULL,
  `grand_total` int(70) NOT NULL,
  `amount_paid` int(50) NOT NULL,
  `balance_amount` int(50) NOT NULL,
  `pay_method` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice`
--

INSERT INTO `invoice` (`id`, `invoice_no`, `vendor`, `rec_date`, `item_name`, `item_model`, `brand_name`, `item_quantity`, `wholesale_price`, `discount_price`, `net_amount`, `total_amount`, `transport_charges`, `other_charges`, `total_invoice_amount`, `pre_pay_method`, `pre_amount`, `this_invoice`, `grand_total`, `amount_paid`, `balance_amount`, `pay_method`) VALUES
(1, '1122334', '', '2017-06-04', 'Array', 'Array', 'Array', '0', '0', '0', '0', '18000', 200, 800, 17000, 'debit', 0, 17000, 17000, 17000, 0, 'cash'),
(2, '3322', 'HTC', '2017-06-04', 'bbb, aaa', 'ccc, fff', 'eee, ddd', '40', '400', '90', '12400', '21400', 300, 200, 20900, 'debit', 10000, 20900, 1500, 1500, 0, 'cash'),
(3, '222', 'HTC', '2017-06-04', 'test22, test', 'test22, testM', 'test22, testB', '10, 30', '2000, 500', '400, 200', '16000, 9000', '25000', 200, 200, 24600, 'debit', 100000, 24600, 75400, 1400, 0, 'cash'),
(4, '3343', 'HTC', '2017-06-04', 'MyItem', 'dfd', 'sdfs', '44', '3555', '344', '141284', '141284', 333, 333, 140618, 'debit', 3333333, 140618, 3192715, 33333, 3159382, 'cash');

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(11) NOT NULL,
  `item_name` varchar(150) NOT NULL,
  `category` varchar(150) NOT NULL,
  `brand` varchar(150) NOT NULL,
  `item_price` varchar(100) NOT NULL,
  `wholesale_price` varchar(100) NOT NULL,
  `retail_price` varchar(100) NOT NULL,
  `profit_margin` varchar(50) NOT NULL,
  `branch` varchar(100) NOT NULL,
  `vendor_id` varchar(20) NOT NULL,
  `item_quantity` varchar(20) NOT NULL,
  `pay_method` varchar(20) NOT NULL,
  `cheque` varchar(30) NOT NULL,
  `total_price` varchar(20) NOT NULL,
  `price_paid` varchar(20) NOT NULL,
  `purchase_date` tinytext NOT NULL,
  `date_paid` tinytext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `item_name`, `category`, `brand`, `item_price`, `wholesale_price`, `retail_price`, `profit_margin`, `branch`, `vendor_id`, `item_quantity`, `pay_method`, `cheque`, `total_price`, `price_paid`, `purchase_date`, `date_paid`) VALUES
(2, 'Iphone', 'Home Appliance', 'Apple', '95000', '81000', '85000', '', 'Rawalpindi', '', '', '', '', '', '', '', ''),
(5, 'LED', 'LCDS', 'Samsung', '56000', '52000', '60000', '', 'Rawalpindi', '1', '10', 'on_cash', '', '560000', '56000', '2017-05-23', '2017-05-23'),
(6, 'MyItem', '', 'Samsung', '158238.08', '3555', '', '18988.5696', '', 'HTC', '44', '', '', '', '', '', '2017-06-04');

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE `sales` (
  `id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_price` int(30) NOT NULL,
  `sale_type` varchar(50) NOT NULL,
  `item_name` text NOT NULL,
  `category` varchar(100) NOT NULL,
  `brand` varchar(150) NOT NULL,
  `branch` varchar(100) NOT NULL,
  `cust_cnic` varchar(150) NOT NULL,
  `cust_name` varchar(150) NOT NULL,
  `present_address` text NOT NULL,
  `permanent_address` text NOT NULL,
  `gur_name1` varchar(100) NOT NULL,
  `gur_cnic1` varchar(50) NOT NULL,
  `gur_name2` varchar(60) NOT NULL,
  `gur_cnic2` varchar(20) NOT NULL,
  `down_payment` varchar(100) NOT NULL,
  `mon_installment` varchar(100) NOT NULL,
  `time_period` varchar(20) NOT NULL,
  `quantity` int(20) NOT NULL,
  `cust_phone` varchar(40) NOT NULL,
  `father_name` varchar(50) NOT NULL,
  `age` varchar(20) NOT NULL,
  `marital_status` varchar(30) NOT NULL,
  `children` varchar(20) NOT NULL,
  `qualification` varchar(100) NOT NULL,
  `accommodation` varchar(30) NOT NULL,
  `off_address` text NOT NULL,
  `working_period` varchar(50) NOT NULL,
  `pre_salary` varchar(50) NOT NULL,
  `other_benefits` varchar(100) NOT NULL,
  `income_others` varchar(100) NOT NULL,
  `income_nature` varchar(100) NOT NULL,
  `work_designation` varchar(40) NOT NULL,
  `work_years` varchar(20) NOT NULL,
  `gur_father_name1` varchar(60) NOT NULL,
  `gur_father_name2` varchar(60) NOT NULL,
  `gur_res_address1` text NOT NULL,
  `gur_res_address2` text NOT NULL,
  `gur_office_address1` text NOT NULL,
  `gur_office_address2` text NOT NULL,
  `gur_res_phone1` varchar(30) NOT NULL,
  `gur_res_phone2` varchar(30) NOT NULL,
  `gur_mobile1` varchar(30) NOT NULL,
  `gur_mobile2` varchar(30) NOT NULL,
  `gur_job_nature1` varchar(100) NOT NULL,
  `gur_job_nature2` varchar(100) NOT NULL,
  `gur_designation1` varchar(40) NOT NULL,
  `gur_designation2` varchar(40) NOT NULL,
  `price_details` text NOT NULL,
  `admin_fee` varchar(30) NOT NULL,
  `first_payment` varchar(50) NOT NULL,
  `surcharge` varchar(30) NOT NULL,
  `total_price` varchar(150) NOT NULL,
  `amount_payable` varchar(150) NOT NULL,
  `total_item_price` varchar(100) NOT NULL,
  `rebate` varchar(200) NOT NULL,
  `barcodes` tinytext NOT NULL,
  `purchase_date` tinytext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`id`, `item_id`, `item_price`, `sale_type`, `item_name`, `category`, `brand`, `branch`, `cust_cnic`, `cust_name`, `present_address`, `permanent_address`, `gur_name1`, `gur_cnic1`, `gur_name2`, `gur_cnic2`, `down_payment`, `mon_installment`, `time_period`, `quantity`, `cust_phone`, `father_name`, `age`, `marital_status`, `children`, `qualification`, `accommodation`, `off_address`, `working_period`, `pre_salary`, `other_benefits`, `income_others`, `income_nature`, `work_designation`, `work_years`, `gur_father_name1`, `gur_father_name2`, `gur_res_address1`, `gur_res_address2`, `gur_office_address1`, `gur_office_address2`, `gur_res_phone1`, `gur_res_phone2`, `gur_mobile1`, `gur_mobile2`, `gur_job_nature1`, `gur_job_nature2`, `gur_designation1`, `gur_designation2`, `price_details`, `admin_fee`, `first_payment`, `surcharge`, `total_price`, `amount_payable`, `total_item_price`, `rebate`, `barcodes`, `purchase_date`) VALUES
(1, 3, 95000, 'on_installment', 'Iphone 5s', 'Mobiles', 'Apple', 'Rawalpindi', '4444444444444', 'MyName', '', '', '', '', 'dfsdfs', '4444444444444', '0', '0', '', 1, '44444444444', 'MyName', '33', 'sdsdfsdf', '4', 'ssdfsdf', '', '', '', '', '', '', '', '', '', '', 'dfssdfs', '', 'dfsdfsdf', '', 'fsdfsdfsdf', '', '44444444444', '', '44444444444', '', 'sdfsdfsd', '', 'fsdfsdfsdf', 'Price is 30000, From Mobile, And Brand is Apple.Price is 95000, From Home Appliance, And Brand is Apple', '0', '0', 'RS 1000', '125000', '125000', '125000', '0', 'barcodes/4444444444444.png', '5/17/17'),
(2, 3, 0, 'on_installment', 'Iphone 5s', 'Home Appliance', 'Apple', 'Rawalpindi', '', 'MyName', '', '', '', '', 'sdsdfdfs', '4444444444444', '0', '0', '', 1, '', 'MyName', '33', '', '', '', '', '', '', '', '', '', '', '', '', 'kjhkjh', 'jhkjh', 'kjhkjhkj', 'hjkhjkhkj', '', 'dfgdgdfg', '', '44444444444', '', '44444444444', '', '444gdfgdfg', '', 'gdfgdf', '', '0', '0', 'RS 1000', '', '', '', '0', 'barcodes/.png', '5/12/17'),
(3, 3, 0, 'on_installment', 'Iphone 5s', 'Mobile', 'Apple', 'Rawalpindi', '1234567891012', 'MyName222', '', '', '', '', 'sdfsfsdfsdf', '6666666666666', '0', '0', '', 1, '', 'MyName222', '33', '', '', '', '', '', '', '', '', '', '', '', '', '', 'sdfsdfsdfsdf', '', 'dfsdfsdf', '', 'sdfsdfsdf', '', '44444444444', '', '44444444444', '', '44444sdfsdfsdf', '', 'sfsdfsdf', '', '0', '0', 'RS 1000', '', '', '', '0', 'barcodes/1234567891012.png', '5/15/17'),
(4, 3, 95000, 'on_cash', 'Iphone 5s, Iphone, ', 'Mobile', 'Apple', 'Rawalpindi', '4444444444444', 'sdfsdfsf', 'sdaf sdfsadfas dfsf ', 'sdfsdf sdfs fsdf', '', '', '', '', '', '', '', 0, '44444444444', 'sdfsdfdsf', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Price is 30000, From Mobile, And Brand is Apple.Price is 95000, From Home Appliance, And Brand is Apple', '', '', '', '125000', '', '125000', '', 'barcodes/4444444444444.png', '5/22/17'),
(5, 3, 30000, 'on_cash', 'Iphone, Iphone 5s, ', 'Mobile', 'Apple', 'Rawalpindi', '4444444444444', 'asfsf', 'sdfasdfsdf fsfasf', 'asfsdf fsdfsdf', '', '', '', '', '', '', '', 1, '44444444444', 'sdfsdf', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Price is 95000, From Home Appliance, And Brand is Apple.Price is 30000, From Mobile, And Brand is Apple', '', '', '', '125000', '', '125000', '', 'barcodes/4444444444444.png', '5/22/17'),
(6, 3, 30000, 'on_cash', 'Iphone, Iphone 5s, ', 'Mobile', 'Apple', 'Rawalpindi', '3333333333333', 'sfsfsf', 'dfsdfasd fsdf sdfs df', 'dfasfasdf sdfas df', '', '', '', '', '', '', '', 2, '33333333333', 'sfsfsfsdf', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Price is 95000, From Home Appliance, And Brand is Apple.Price is 30000, From Mobile, And Brand is Apple', '', '', '', '125000', '', '125000', '', 'barcodes/3333333333333.png', '5/22/17'),
(7, 5, 95000, 'on_cash', 'Iphone 5s, Iphone, ', 'LCDS', 'Samsung', 'Rawalpindi', '344443-5-2323', 'Sultan Ahmed', '', 'H. No. 3, Street 6, Murree Road', '', '', '', '', '', '', '', 2, '+92387646347', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Price is 30000, From Mobile, And Brand is Apple.Price is 95000, From Home Appliance, And Brand is Apple', '', '', '', '125000', '', '125000', '', 'barcodes/344443-5-2323.png', '5/25/17'),
(8, 5, 2000, 'free', 'Oven, ', 'LCDS', 'Samsung', 'Rawalpindi', '344443-5-2323', 'Sultan Ahmed', '', '', '', '', '', '', '', '', '', 1, '+92387646347', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Price is 2000, From Home Appliance, And Brand is Hitachi', '', '', '', '2000', '', '2000', '', 'barcodes/344443-5-2323.png', '5/25/17');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `role` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL,
  `branch` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `user_name`, `password`, `role`, `phone`, `status`, `branch`) VALUES
(1, 'jawad', 'sahi', 'jawad', 'jawad1', 'admin', '', '', ''),
(10, 'test', 'test', 'salesman', 'test', 'sales_man', '4534535345', '', 'Rawalpindi'),
(15, 'Muhammad', 'Hanif', 'hanif', '12345', 'admin', 'hanif@abc.com', '', 'Rawalpindi'),
(16, 'Manager', 'Sab', 'manager', 'manager', 'manager', 'manager@sabasellers.com', '', 'Rawalpindi'),
(17, 'M', 'Saleem', 'salesman', '12345', 'sales_man', '03018480357', 'Active', 'Lahore Mall');

-- --------------------------------------------------------

--
-- Table structure for table `vendors`
--

CREATE TABLE `vendors` (
  `id` int(11) NOT NULL,
  `vendor_name` varchar(100) NOT NULL,
  `v_address` tinytext NOT NULL,
  `v_city` varchar(60) NOT NULL,
  `v_phone` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendors`
--

INSERT INTO `vendors` (`id`, `vendor_name`, `v_address`, `v_city`, `v_phone`) VALUES
(1, 'HTC', 'Street No 4, New Heights, NYC', 'New York', '03018480357'),
(2, '', '', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `branches`
--
ALTER TABLE `branches`
  ADD PRIMARY KEY (`branch_id`);

--
-- Indexes for table `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clear_installments`
--
ALTER TABLE `clear_installments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `installment_plans`
--
ALTER TABLE `installment_plans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendors`
--
ALTER TABLE `vendors`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `branches`
--
ALTER TABLE `branches`
  MODIFY `branch_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `brand`
--
ALTER TABLE `brand`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `clear_installments`
--
ALTER TABLE `clear_installments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `installment_plans`
--
ALTER TABLE `installment_plans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `vendors`
--
ALTER TABLE `vendors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
