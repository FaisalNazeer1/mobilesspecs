<!DOCTYPE html>
<html lang="en">

<head>
	<title>Mobile Planet</title>

	<!-- Meta -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	<!-- Favicon -->
	<link rel="shortcut icon" href="favicon.ico">

	<!-- Web Fonts -->
	<link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,800&amp;subset=cyrillic,latin'>

	<!-- CSS Global Compulsory -->
	<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/shop.style.css">

	<!-- CSS Header and Footer -->
	<link rel="stylesheet" href="assets/css/headers/header-v5.css">
	<link rel="stylesheet" href="assets/css/footers/footer-v4.css">

	<!-- CSS Implementing Plugins -->
	<link rel="stylesheet" href="assets/plugins/animate.css">
	<link rel="stylesheet" href="assets/plugins/line-icons/line-icons.css">
	<link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/plugins/scrollbar/css/jquery.mCustomScrollbar.css">
	<link rel="stylesheet" href="assets/plugins/owl-carousel/owl-carousel/owl.carousel.css">
	<link rel="stylesheet" href="assets/plugins/revolution-slider/rs-plugin/css/settings.css">

	<!-- CSS Theme -->
	<link rel="stylesheet" href="assets/css/theme-colors/default.css" id="style_color">
	<link rel="stylesheet" href="assets/plugins/ladda-buttons/css/custom-lada-btn.css">
	<link rel="stylesheet" href="assets/plugins/hover-effects/css/custom-hover-effects.css">
	
	<!-- CSS Customization -->
	<link rel="stylesheet" href="assets/css/custom.css">
<style type="text/css">
/*Breadcrumbs v4
------------------------------------*/
.breadcrumbs-v4 {
  width: 100%;
  padding: 60px 0;
  position: relative;
  background-position: top left;
  background-repeat: no-repeat;
  background-image: url(smartphones/androidphones2.png);
  background-size:cover;
}

.breadcrumbs-v4:before {
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  content: " ";
  position: absolute;
  background: rgba(0,0,0,0.3);
}

.breadcrumbs-v4 .container {
  position: relative;
}

.breadcrumbs-v4 span.page-name {
  color: #fff;
  display: block;
  font-size: 18px;
  font-weight: 200;
  margin: 0 0 5px 3px;
}

.breadcrumbs-v4 h1 {
  color: #fff;
  font-size: 40px;
  font-weight: 200;
  margin: 0 0 20px;
  line-height: 50px;
  text-transform: uppercase;
}

.breadcrumbs-v4 .breadcrumb-v4-in {
  padding-left: 0;
  margin-bottom: 0;
  list-style: none;
}

.breadcrumbs-v4 .breadcrumb-v4-in > li {
  color: #fff;
  font-size: 13px;
  display: inline-block;
}

.breadcrumbs-v4 .breadcrumb-v4-in > li + li:before {
  color: #fff;
  content: "\f105";
  margin-left: 7px;
  padding-right: 8px;
  font-family: FontAwesome;
}

.breadcrumbs-v4 .breadcrumb-v4-in li a {
  color: #fff;
}

.breadcrumbs-v4 .breadcrumb-v4-in li.active,
.breadcrumbs-v4 .breadcrumb-v4-in li a:hover {
  color: #18ba9b;
  text-decoration: none;
}

@media (max-width: 768px) {
  .breadcrumbs-v4 {
    text-align: center;
  }

  .breadcrumbs-v4 span.page-name {
    font-size: 18px;
  }

  .breadcrumbs-v4 h1 {
    font-size: 30px;
    margin-bottom: 10px;
  }
}
#mobile_overview::-webkit-scrollbar { 
    display: none; 
}
#mobile_overview {
    height: 100%;
}

object {
    width: 100%;
    min-height: 100%;
}
li > a {
	color: white !important; font-weight: 600;
}
.content_image_main_gradient {
  background: #3e5ba6;
  background: -moz-linear-gradient(top, #dedbd4 1%, #3e5ba6 60%);
  background: -webkit-gradient(linear, left top, left bottom, color-stop(1%, #dedbd4), color-stop(60%, #3e5ba6));
  background: -webkit-linear-gradient(top, #dedbd4 1%, #3e5ba6 60%);
  background: -o-linear-gradient(top, #dedbd4 1%, #3e5ba6 60%);
  background: -ms-linear-gradient(top, #dedbd4 1%, #3e5ba6 60%);
  background: linear-gradient(to bottom, #dedbd4 1%, #3e5ba6 60%);
  background-size: contain;
  background-repeat: no-repeat;
  background-position: center;
}
</style>
</head>

<body class="header-fixed">

	<div class="wrapper">

	<?php include("header.php"); ?>
	<!--=== Breadcrumbs v4 ===-->
 		<div class="breadcrumbs-v4">
 			<div class="container">
 				<!-- <span class="page-name">Product Filter Page</span> -->
 				<h1>Samsung Galaxy <span class="shop-red"><strong>S7</strong></span></h1>
 				<!-- <ul class="breadcrumb-v4-in">
 					<li><a href="index.html">Home</a></li>
 					<li><a href="#">Product</a></li>
 					<li class="active">Product Filter Page</li>
 				</ul> -->
 			</div><!--/end container-->
 		</div>
 		<!--=== End Breadcrumbs v4 ===-->

	<div style="margin-top: -30px;" class="content container">
 		<?php include("sidebar.php"); ?>
 				<div class="col-md-9">
 					<div class="filter-results">
 						<div class="row illustration-v2 margin-bottom-30">
 							<div style="height: 2000px; width: 769px;" class="col-md-9" id="mobile_overview">
 				
 							</div>
 						</div>
 					</div>
 				</div>
 	<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
 	<script type="text/javascript">
    		/*document.getElementById("mobile_overview").innerHTML='<object type="text/html" data="mobile_overview.html" ></object>';*/
    		$(document).ready( function() {
    			$("#mobile_overview").css("overflow","hidden");
        		$("#mobile_overview").html('<object data="mobile_overview.html" width="auto" height="auto" style="overflow:auto;border:3px ridge blue"/>');
			});
 	</script>
 	</div><!--/end row-->
 	</div><!--/end container-->

	<?php include("footer.php"); ?>
</div><!--/wrapper-->

<!-- JS Global Compulsory -->
<script src="assets/plugins/jquery/jquery.min.js"></script>
<script src="assets/plugins/jquery/jquery-migrate.min.js"></script>
<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/plugins/noUiSlider/jquery.nouislider.all.min.js"></script>
<!-- JS Implementing Plugins -->
<script src="assets/plugins/back-to-top.js"></script>
<script src="assets/plugins/smoothScroll.js"></script>
<script src="assets/plugins/jquery.parallax.js"></script>
<script src="assets/plugins/owl-carousel/owl-carousel/owl.carousel.js"></script>
<script src="assets/plugins/scrollbar/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="assets/plugins/revolution-slider/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script src="assets/plugins/revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script src="assets/plugins/sky-forms-pro/skyforms/js/jquery-ui.min.js"></script>
<!-- JS Customization -->
<script src="assets/js/custom.js"></script>
<!-- JS Page Level -->
<script src="assets/js/shop.app.js"></script>
<script src="assets/js/plugins/owl-carousel.js"></script>
<script src="assets/js/plugins/revolution-slider.js"></script>
<script src="assets/js/plugins/style-switcher.js"></script>
<script type="text/javascript" src="assets/js/plugins/ladda-buttons.js"></script>
<script>
	jQuery(document).ready(function() {
		App.init();
		App.initScrollBar();
		App.initParallaxBg();
		OwlCarousel.initOwlCarousel();
		RevolutionSlider.initRSfullWidth();
		//StyleSwitcher.initStyleSwitcher();
	});
</script>

</body>

</html>
