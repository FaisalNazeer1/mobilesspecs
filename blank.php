<!DOCTYPE html>
<html lang="en">

<head>
	<title>Mobile Planet</title>

	<!-- Meta -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	<!-- Favicon -->
	<link rel="shortcut icon" href="favicon.ico">

	<!-- Web Fonts -->
	<link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,800&amp;subset=cyrillic,latin'>

	<!-- CSS Global Compulsory -->
	<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/shop.style.css">

	<!-- CSS Header and Footer -->
	<link rel="stylesheet" href="assets/css/headers/header-v5.css">
	<link rel="stylesheet" href="assets/css/footers/footer-v4.css">

	<!-- CSS Implementing Plugins -->
	<link rel="stylesheet" href="assets/plugins/animate.css">
	<link rel="stylesheet" href="assets/plugins/line-icons/line-icons.css">
	<link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/plugins/scrollbar/css/jquery.mCustomScrollbar.css">
	<link rel="stylesheet" href="assets/plugins/owl-carousel/owl-carousel/owl.carousel.css">
	<link rel="stylesheet" href="assets/plugins/revolution-slider/rs-plugin/css/settings.css">

	<!-- CSS Theme -->
	<link rel="stylesheet" href="assets/css/theme-colors/default.css" id="style_color">
	<link rel="stylesheet" href="assets/plugins/ladda-buttons/css/custom-lada-btn.css">
	<link rel="stylesheet" href="assets/plugins/hover-effects/css/custom-hover-effects.css">

	<!-- CSS Customization -->
	<link rel="stylesheet" href="assets/css/custom.css">
<style type="text/css">
.price {
	font-weight: 700 !important;
	color: #999;
	font-size: 16px;
	width: 220px !important;
	border: 2px solid #999;
	border-radius: 4px;
}
.price:hover {
	color: darkred;
	border: 2px solid green;
	border-radius: 8px;
}
</style>
</head>

<body class="header-fixed">

	<div class="wrapper">

	<?php include("header.php"); ?>

	<div class="content container">
 			<div class="row">
 				<div class="col-md-3 filter-by-block md-margin-bottom-60">
 					<h1>SmartPhone Finder</h1>
 					<div class="panel-group" id="accordion">
 						<div class="panel panel-default">
 							<div class="panel-heading">
 								<h2 class="panel-title">
 									<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
 										Brands
 										<i class="fa fa-angle-down"></i>
 									</a>
 								</h2>
 							</div>
 							<div id="collapseOne" class="panel-collapse collapse in">
 								<div class="panel-body">
 									<ul class="list-unstyled checkbox-list">
 										<li>
 											<a style="width: 218px; text-align: center;" href="" class="btn-u btn-brd btn-brd-hover rounded-2x btn-u-red btn-u-lg pulse-shrink">iPhone</a>
 										</li>
 										<li>
 											<a style="width: 218px; text-align: center;" href="" class="btn-u btn-brd btn-brd-hover rounded-2x btn-u-blue btn-u-lg pulse-shrink">Samsung</a>
 										</li>
 										<li>
 											<a style="width: 218px; text-align: center;" href="" class="btn-u btn-brd btn-brd-hover rounded-2x btn-u-orange btn-u-lg pulse-shrink">HTC</a>
 										</li>
 										<li>
 											<a style="width: 218px; text-align: center;" href="" class="btn-u btn-brd btn-brd-hover rounded-2x btn-u-yellow btn-u-lg pulse-shrink">LG</a>
 										</li>
 										<li>
 											<a style="width: 218px; text-align: center;" href="" class="btn-u btn-brd btn-brd-hover rounded-2x btn-u-green btn-u-lg pulse-shrink">Sony</a>
 										</li>
 										<li>
 											<a style="width: 218px; text-align: center;" href="" class="btn-u btn-brd btn-brd-hover rounded-2x btn-u-red btn-u-lg pulse-shrink">QMobile</a>
 										</li>
 										<li>
 											<a style="width: 218px; text-align: center;" href="" class="btn-u btn-brd btn-brd-hover rounded-2x btn-u-blue btn-u-lg pulse-shrink">Huawei</a>
 										</li>
 										<li>
 											<a style="width: 218px; text-align: center;" href="" class="btn-u btn-brd btn-brd-hover rounded-2x btn-u-green btn-u-lg pulse-shrink">All Brands</a>
 										</li>
 									</ul>
 								</div>
 							</div>
 						</div>
 					</div><!--/end panel group-->

 					<div class="panel-group" id="accordion-v2">
 						<div class="panel panel-default">
 							<div class="panel-heading">
 								<h2 class="panel-title">
 									<a data-toggle="collapse" data-parent="#accordion-v2" href="#collapseTwo">
 										Search By Price
 										<i class="fa fa-angle-down"></i>
 									</a>
 								</h2>
 							</div>
 							<div id="collapseTwo" class="panel-collapse collapse in">
 								<div class="panel-body">
 									<ul class="list-unstyled checkbox-list">
 										<li>
 											<label class="checkbox">
 												<input type="checkbox" name="checkbox" checked />
 												<i></i>
 												iPhone
 												<!-- <small><a href="#">(23)</a></small> -->
 											</label>
 										</li>
 										<li>
 											<label class="checkbox">
 												<input type="checkbox" name="checkbox" />
 												<i></i>
 												Samsung
 												<!-- <small><a href="#">(4)</a></small> -->
 											</label>
 										</li>
 										<li>
 											<label class="checkbox">
 												<input type="checkbox" name="checkbox" />
 												<i></i>
 												HTC
 												<!-- <small><a href="#">(4)</a></small> -->
 											</label>
 										</li>
 										<li>
 											<label class="checkbox">
 												<input type="checkbox" name="checkbox" />
 												<i></i>
 												LG
 												<!-- <small><a href="#">(11)</a></small> -->
 											</label>
 										</li>
 										<li>
 											<label class="checkbox">
 												<input type="checkbox" name="checkbox" />
 												<i></i>
 												Sony
 												<!-- <small><a href="#">(11)</a></small> -->
 											</label>
 										</li>
 										<li>
 											<label class="checkbox">
 												<input type="checkbox" name="checkbox" />
 												<i></i>
 												QMobile
 												<!-- <small><a href="#">(11)</a></small> -->
 											</label>
 										</li>
 										<li>
 											<label class="checkbox">
 												<input type="checkbox" name="checkbox" />
 												<i></i>
 												Huawei
 												<!-- <small><a href="#">(11)</a></small> -->
 											</label>
 										</li>
 										<li>
									<label class="select">
									<select class="price">
										<option value="0" selected="" disabled="">Choose Price</option>
										<option value="a">0 - 5,000</option>
										<option value="b">5,001 - 10,000</option>
										<option value="c">10,000 - 15,000</option>
										<option value="d">15,001 - 20,000</option>
										<option value="e">20,001 - 25,000</option>
										<option value="f">25,001 - 30,000</option>
										<option value="g">Above 30,000</option>
									</select>
									<i></i>
								</label>
								</li>
										<li>
 											<a style="width: 218px; text-align: center; font-weight: 600;" href="" class="btn-u btn-brd btn-brd-hover rounded-2x btn-u-green btn-u-lg">Submit</a>
 										</li>
 									</ul>
 								</div>
 							</div>
 						</div>
 					</div><!--/end panel group-->

 					<div class="panel-group" id="accordion-v3">
 						<div class="panel panel-default">
 							<div class="panel-heading">
 								<h2 class="panel-title">
 									<a data-toggle="collapse" data-parent="#accordion-v3" href="#collapseThree">
 										Search By Specs
 										<i class="fa fa-angle-down"></i>
 									</a>
 								</h2>
 							</div>
 							<div id="collapseThree" class="panel-collapse collapse in">
 								<div class="panel-body">
 									<ul class="list-unstyled checkbox-list">
 										<li>
 											<label class="checkbox">
 												<input type="checkbox" name="checkbox" checked />
 												<i></i>
 												Camera
 												<!-- <small><a href="#">(23)</a></small> -->
 											</label>
 										</li>
 										<li>
 											<label class="checkbox">
 												<input type="checkbox" name="checkbox" />
 												<i></i>
 												RAM
 												<!-- <small><a href="#">(4)</a></small> -->
 											</label>
 										</li>
 										<li>
 											<label class="checkbox">
 												<input type="checkbox" name="checkbox" />
 												<i></i>
 												ROM
 												<!-- <small><a href="#">(11)</a></small> -->
 											</label>
 										</li>
 										<!-- <li>
 											<label class="checkbox">
 												<input type="checkbox" name="checkbox" />
 												<i></i>
 												XL
 												<small><a href="#">(3)</a></small>
 											</label>
 										</li> -->
 									</ul>
 								</div>
 							</div>
 						</div>
 					</div><!--/end panel group-->

 					<div class="panel-group" id="accordion-v4">
 						<div class="panel panel-default">
 							<div class="panel-heading">
 								<h2 class="panel-title">
 									<a data-toggle="collapse" data-parent="#accordion-v4" href="#collapseFour">
 										Brand Comparison
 										<i class="fa fa-angle-down"></i>
 									</a>
 								</h2>
 							</div>
 							<div id="collapseFour" class="panel-collapse collapse in">
 								<div class="panel-body">
 									<ul class="list-unstyled checkbox-list">
 										<li>
 											<a style="width: 218px; text-align: center; font-weight: 600;" href="" class="btn-u btn-brd btn-brd-hover btn-u-red round-corners btn-u-lg">Compare</a>
 										</li>
 									</ul>
 								</div>
 							</div>
 						</div>
 					</div>

 					<div class="panel-group" id="accordion-v5">
 						<div class="panel panel-default">
 							<div class="panel-heading">
 								<h2 class="panel-title">
 									<a data-toggle="collapse" data-parent="#accordion-v5" href="#collapseFive">
 										Latest Additions
 										<i class="fa fa-angle-down"></i>
 									</a>
 								</h2>
 							</div>
 							<div id="collapseFive" class="panel-collapse collapse in">
 								<div class="panel-body">
 									<ul class="list-unstyled checkbox-list">
 										<li><a href="#"><img src="img/product/mobile/1.jpg" width="50px" height="50px" alt=""><span style="margin-left: 20px; font-weight: 700;color: #999; font-size: 14px; line-height: 25px; font-family: 'Open Sans', sans-serif;">iPhone 6s</span></a></li>
 										<li><a href="#"><img src="img/product/mobile/3.jpg" width="50px" height="50px" alt=""><span style="margin-left: 20px; font-weight: 700;color: #999; font-size: 14px; line-height: 25px; font-family: 'Open Sans', sans-serif;">iPhone 5s</span></a></li>
 										<li><a href="#"><img src="img/product/mobile/5.jpg" width="50px" height="50px" alt=""><span style="margin-left: 20px; font-weight: 700;color: #999; font-size: 14px; line-height: 25px; font-family: 'Open Sans', sans-serif;">Samsung Galaxy S7</span></a></li>
 										<li><a href="#"><img src="img/product/mobile/9.jpg" width="50px" height="50px" alt=""><span style="margin-left: 20px; font-weight: 700;color: #999; font-size: 14px; line-height: 25px; font-family: 'Open Sans', sans-serif;">Apple Iphone 6</span></a></li>
 										<li><a href="#"><img src="img/product/mobile/7.jpg" width="50px" height="50px" alt=""><span style="margin-left: 20px; font-weight: 700;color: #999; font-size: 14px; line-height: 25px; font-family: 'Open Sans', sans-serif;">HTC one M</span></a></li>
 									</ul>
 								</div>
 							</div>
 						</div>
 					</div>

 					<div class="panel-group margin-bottom-30" id="accordion-v6">
 						<div class="panel panel-default">
 							<div class="panel-heading">
 								<h2 class="panel-title">
 									<a data-toggle="collapse" data-parent="#accordion-v6" href="#collapseSix">
 										Top 10 Smartphones of 2017
 										<i class="fa fa-angle-down"></i>
 									</a>
 								</h2>
 							</div>
 							<div id="collapseSix" class="panel-collapse collapse in">
 								<div class="panel-body">
 									<ul class="list-unstyled checkbox-list">
 										<li><a href="#"><img src="img/product/mobile/1.jpg" width="50px" height="50px" alt=""><span style="margin-left: 20px; font-weight: 700;color: #999; font-size: 14px; line-height: 25px; font-family: 'Open Sans', sans-serif;">iPhone 6s</span></a></li>
 										<li><a href="#"><img src="img/product/mobile/3.jpg" width="50px" height="50px" alt=""><span style="margin-left: 20px; font-weight: 700;color: #999; font-size: 14px; line-height: 25px; font-family: 'Open Sans', sans-serif;">iPhone 5s</span></a></li>
 										<li><a href="#"><img src="img/product/mobile/5.jpg" width="50px" height="50px" alt=""><span style="margin-left: 20px; font-weight: 700;color: #999; font-size: 14px; line-height: 25px; font-family: 'Open Sans', sans-serif;">Samsung Galaxy S7</span></a></li>
 										<li><a href="#"><img src="img/product/mobile/9.jpg" width="50px" height="50px" alt=""><span style="margin-left: 20px; font-weight: 700;color: #999; font-size: 14px; line-height: 25px; font-family: 'Open Sans', sans-serif;">Apple Iphone 6</span></a></li>
 										<li><a href="#"><img src="img/product/mobile/7.jpg" width="50px" height="50px" alt=""><span style="margin-left: 20px; font-weight: 700;color: #999; font-size: 14px; line-height: 25px; font-family: 'Open Sans', sans-serif;">HTC one M</span></a></li>
 										<li><a href="#"><img src="img/product/mobile/11.jpg" width="50px" height="50px" alt=""><span style="margin-left: 20px; font-weight: 700;color: #999; font-size: 14px; line-height: 25px; font-family: 'Open Sans', sans-serif;">Sony Xperia Z5</span></a></li>
 										<li><a href="#"><img src="img/product/mobile/13.jpg" width="50px" height="50px" alt=""><span style="margin-left: 20px; font-weight: 700;color: #999; font-size: 14px; line-height: 25px; font-family: 'Open Sans', sans-serif;">Samsung Duos</span></a></li>
 										<li><a href="#"><img src="img/product/mobile/15.jpg" width="50px" height="50px" alt=""><span style="margin-left: 20px; font-weight: 700;color: #999; font-size: 14px; line-height: 25px; font-family: 'Open Sans', sans-serif;">Samsung S7 Edge</span></a></li>
 										<li><a href="#"><img src="img/product/mobile/17.jpg" width="50px" height="50px" alt=""><span style="margin-left: 20px; font-weight: 700;color: #999; font-size: 14px; line-height: 25px; font-family: 'Open Sans', sans-serif;">Samsung Note 5</span></a></li>
 										<li><a href="#"><img src="img/product/mobile/xx.png" width="50px" height="50px" alt=""><span style="margin-left: 20px; font-weight: 700;color: #999; font-size: 14px; line-height: 25px; font-family: 'Open Sans', sans-serif;">Sony Xperia XZS</span></a></li>
 									</ul>
 								</div>
 							</div>
 						</div>
 					</div>

 					<div class="panel-group margin-bottom-30" id="accordion-v7">
 						<div class="panel panel-default">
 							<div class="panel-heading">
 								<h2 class="panel-title">
 									<a data-toggle="collapse" data-parent="#accordion-v7" href="#collapseSeven">
 										Latest Smartphones
 										<i class="fa fa-angle-down"></i>
 									</a>
 								</h2>
 							</div>
 							<div id="collapseSeven" class="panel-collapse collapse in">
 								<div class="panel-body">
 									<ul class="list-unstyled checkbox-list">
 										<li><a href="#"><img src="img/product/mobile/5.jpg" width="50px" height="50px" alt=""><span style="margin-left: 20px; font-weight: 700;color: #999; font-size: 14px; line-height: 25px; font-family: 'Open Sans', sans-serif;">Samsung Galaxy S7</span></a></li>
 										<li><a href="#"><img src="img/product/mobile/7.jpg" width="50px" height="50px" alt=""><span style="margin-left: 20px; font-weight: 700;color: #999; font-size: 14px; line-height: 25px; font-family: 'Open Sans', sans-serif;">HTC one M</span></a></li>
 										<li><a href="#"><img src="img/product/mobile/11.jpg" width="50px" height="50px" alt=""><span style="margin-left: 20px; font-weight: 700;color: #999; font-size: 14px; line-height: 25px; font-family: 'Open Sans', sans-serif;">Sony Xperia Z5</span></a></li>
 										<li><a href="#"><img src="img/product/mobile/15.jpg" width="50px" height="50px" alt=""><span style="margin-left: 20px; font-weight: 700;color: #999; font-size: 14px; line-height: 25px; font-family: 'Open Sans', sans-serif;">Samsung S7 Edge</span></a></li>
 										<li><a href="#"><img src="img/product/mobile/17.jpg" width="50px" height="50px" alt=""><span style="margin-left: 20px; font-weight: 700;color: #999; font-size: 14px; line-height: 25px; font-family: 'Open Sans', sans-serif;">Samsung Note 5</span></a></li>
 										<li><a href="#"><img src="img/product/mobile/xx.png" width="50px" height="50px" alt=""><span style="margin-left: 20px; font-weight: 700;color: #999; font-size: 14px; line-height: 25px; font-family: 'Open Sans', sans-serif;">Sony Xperia XZS</span></a></li>
 									</ul>
 								</div>
 							</div>
 						</div>
 					</div>
 				</div> 
 			<div class="col-md-9">
 					<div class="filter-results">
 						<div class="row illustration-v2 margin-bottom-30">
 							
 						</div>
 					</div><!--/end filter resilts-->
 				</div>
 			</div><!--/end row-->
 		</div><!--/end container-->

	<?php include("footer.php"); ?>
</div><!--/wrapper-->

<!-- JS Global Compulsory -->
<script src="assets/plugins/jquery/jquery.min.js"></script>
<script src="assets/plugins/jquery/jquery-migrate.min.js"></script>
<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/plugins/noUiSlider/jquery.nouislider.all.min.js"></script>
<!-- JS Implementing Plugins -->
<script src="assets/plugins/back-to-top.js"></script>
<script src="assets/plugins/smoothScroll.js"></script>
<script src="assets/plugins/jquery.parallax.js"></script>
<script src="assets/plugins/owl-carousel/owl-carousel/owl.carousel.js"></script>
<script src="assets/plugins/scrollbar/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="assets/plugins/revolution-slider/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script src="assets/plugins/revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script src="assets/plugins/sky-forms-pro/skyforms/js/jquery-ui.min.js"></script>
<!-- JS Customization -->
<script src="assets/js/custom.js"></script>
<!-- JS Page Level -->
<script src="assets/js/shop.app.js"></script>
<script src="assets/js/plugins/owl-carousel.js"></script>
<script src="assets/js/plugins/revolution-slider.js"></script>
<script src="assets/js/plugins/style-switcher.js"></script>
<script type="text/javascript" src="assets/js/plugins/ladda-buttons.js"></script>
<script>
	jQuery(document).ready(function() {
		App.init();
		App.initScrollBar();
		App.initParallaxBg();
		OwlCarousel.initOwlCarousel();
		RevolutionSlider.initRSfullWidth();
		//StyleSwitcher.initStyleSwitcher();
	});
</script>

</body>

</html>
