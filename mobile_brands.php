<?php $brand_name = $_GET['brand_name']; ?>
<!DOCTYPE html>

<html lang="en">



<head>

  <title>Mobile Planet - Mobile Prices in Pakistan</title>



  <!-- Meta -->

  <meta charset="utf-8">

  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <meta name="description" content="">

  <meta name="author" content="">
  <meta name="keywords" content="Mobile Phone Prices">


  <!-- Favicon -->

  <link rel="shortcut icon" href="favicon.ico">



  <!-- Web Fonts -->

  <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,800&amp;subset=cyrillic,latin'>



  <!-- CSS Global Compulsory -->

  <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">

  <link rel="stylesheet" href="assets/css/shop.style.css">



  <!-- CSS Header and Footer -->

  <link rel="stylesheet" href="assets/css/headers/header-v5.css">

  <link rel="stylesheet" href="assets/css/footers/footer-v4.css">



  <!-- CSS Implementing Plugins -->

  <link rel="stylesheet" href="assets/plugins/animate.css">

  <link rel="stylesheet" href="assets/plugins/line-icons/line-icons.css">

  <link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.min.css">

  <link rel="stylesheet" href="assets/plugins/scrollbar/css/jquery.mCustomScrollbar.css">

  <link rel="stylesheet" href="assets/plugins/owl-carousel/owl-carousel/owl.carousel.css">

  <link rel="stylesheet" href="assets/plugins/revolution-slider/rs-plugin/css/settings.css">



  <!-- CSS Theme -->

  <link rel="stylesheet" href="assets/css/theme-colors/default.css" id="style_color">

  <link rel="stylesheet" href="assets/plugins/ladda-buttons/css/custom-lada-btn.css">

  <link rel="stylesheet" href="assets/plugins/hover-effects/css/custom-hover-effects.css">

  <link rel="stylesheet" href="search_css/style.css">

  <script type="text/javascript" src="http://www.google.com/jsapi"></script>

  <script type="text/javascript" src="search_js/script.js"></script>

  

  <!-- CSS Customization -->

  <link rel="stylesheet" href="assets/css/custom.css">
  <link rel="stylesheet" href="assets/css/product_details.css">
  <link rel="stylesheet" href="assets/css/mobile_brands.css">

<style type="text/css">
ul.pagination {
    display: inline-block;
    padding: 0;
    margin: 0;
}

ul.pagination li {display: inline;}

ul.pagination li a {
    color: black !important;
    float: left;
    padding: 8px 16px;
    text-decoration: none;
    transition: background-color .3s;
    border: 1px solid #ddd;
    margin: 0 4px;
}

ul.pagination li a.current {
    background-color: #4CAF50;
    color: white;
    border: 1px solid #4CAF50;
}

ul.pagination li a:hover:not(.current) {background-color: #ddd;}
</style>

</head>



<body class="header-fixed">



  <div class="wrapper">

  <?php include("header_device.php"); ?>

  <!--=== Breadcrumbs v4 ===-->

    <div class="breadcrumbs-v4">

      <div class="container">

        <!-- <span class="page-name">Product Filter Page</span> -->

        <h1 style="font-family: oswald; font-weight: 600; text-shadow: 2px 2px 2px black;"><?php echo mb_strtoupper($brand_name); ?> <span class="shop-red"><strong></strong></span></h1>

        <!-- <ul class="breadcrumb-v4-in">

          <li><a href="index.html">Home</a></li>

          <li><a href="#">Product</a></li>

          <li class="active">Product Filter Page</li>

        </ul> -->

      </div><!--/end container-->

    </div>

    <!--=== End Breadcrumbs v4 ===-->

    <div class="content-md">

    <?php include("sidebar_mobile.php");?>

        <div class="col-md-9">

          <div class="filter-results">

          <h1 style="background-color: rgba(235, 235, 235, 0.57);"><span class="shop-red"><b>MOBILE BRAND:</b></span> <strong style="font-family: oswald"><?php echo mb_strtoupper($brand_name); ?></strong></h1>

          <hr>

            <div class="row illustration-v2 margin-bottom-30">



              <?php include("connect.php");

              if ($brand_name == "All brands") {

                  $brand_name = "__%";

              }else {

              }

            $counter = 1;

            include_once('functions.php');

          $page = (int)(!isset($_GET["page"]) ? 1 : $_GET["page"]);
          if ($page <= 0) $page = 1;

          $per_page = 35; // Set how many records do you want to display per page.

          $startpoint = ($page * $per_page) - $per_page;

          $statement = "`mobiles` WHERE `dev_brand` LIKE '$brand_name%' ORDER BY `id` DESC"; // Change `records` according to your table name.
           
          $results = mysqli_query($con,"SELECT * FROM {$statement} LIMIT {$startpoint} , {$per_page}");

          if (mysqli_num_rows($results) != 0) {

            while ($row = mysqli_fetch_array($results)) {

              $url = $row['url'];

              $mobile_name = $row['dev_name'];

              $price = $row['price'];

              $image = $row['picture'];

              ?>

              <?php

              ?>

              <div class="col-xs-6 col-sm-4 col-md-2 col-lg-2 devices_list product-img-brd">  
                  <div class="device_box_v2 product-img">                        
                        <a title="<?php echo $mobile_name; ?>" target="_<?php echo $url; echo "?m_name=$mobile_name"; ?>" href="<?php echo $url; echo "?m_name=$mobile_name"; ?>"><img style="height: 167.75px !important;" class="img-responsive img-hover" alt="<?php echo $mobile_name; ?>" title="<?php echo $mobile_name; ?>" src="<?php echo $image; ?>" /></a>
                                    
                        <a class="add-to-cart" target="_<?php echo $url; echo "?m_name=$mobile_name"; ?>" href="<?php echo $url; echo "?m_name=$mobile_name"; ?>"><!-- <i class="fa fa-shopping-cart"></i> -->PRICE RS <?php echo $price; ?></a>
                    </div>
                        <a class="product-review" target="_<?php echo $url; echo "?m_name=$mobile_name"; ?>" href="<?php echo $url; echo "?m_name=$mobile_name"; ?>"><?php echo $mobile_name; ?></a>
              </div>

            <?php
              }
            }
            else {
                echo "No records are found.";
            }
            ?>

            </div>
            <hr>
            <div style="text-align: center;">
            <?php echo pagination($statement,$per_page,$page,$url='?'); ?>
            </div>
          </div>

        </div>

  </div>

  <div class="clear"></div>

  

  </div><!--/end wrapper-->
<?php include("footer.php"); ?>
<!-- JS Global Compulsory -->

<script src="assets/plugins/jquery/jquery.min.js"></script>

<script src="assets/plugins/jquery/jquery-migrate.min.js"></script>

<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>

<script src="assets/plugins/noUiSlider/jquery.nouislider.all.min.js"></script>

<!-- JS Implementing Plugins -->

<script src="assets/plugins/back-to-top.js"></script>

<script src="assets/plugins/smoothScroll.js"></script>

<script src="assets/plugins/jquery.parallax.js"></script>

<script src="assets/plugins/owl-carousel/owl-carousel/owl.carousel.js"></script>

<script src="assets/plugins/scrollbar/js/jquery.mCustomScrollbar.concat.min.js"></script>

<script src="assets/plugins/revolution-slider/rs-plugin/js/jquery.themepunch.tools.min.js"></script>

<script src="assets/plugins/revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

<script src="assets/plugins/sky-forms-pro/skyforms/js/jquery-ui.min.js"></script>

<!-- JS Customization -->

<script src="assets/js/custom.js"></script>

<!-- JS Page Level -->

<script src="assets/js/shop.app.js"></script>

<script src="assets/js/plugins/owl-carousel.js"></script>

<script src="assets/js/plugins/revolution-slider.js"></script>

<script>

  jQuery(document).ready(function() {

    App.init();

    App.initScrollBar();

    App.initParallaxBg();

    OwlCarousel.initOwlCarousel();

    RevolutionSlider.initRSfullWidth();

    //StyleSwitcher.initStyleSwitcher();

  });

</script>

</body>

</html>